﻿using System;
using UIKit;
using CoreGraphics;
using Foundation;

namespace AlbaCinema.iOS
{
	public class HorariosCell : UICollectionViewCell
	{
		UILabel headingLabel;

		[Export ("initWithFrame:")]
		public HorariosCell (CGRect frame) : base (frame)
		{
			BackgroundView = new UIView{BackgroundColor = UIColor.Orange};

			SelectedBackgroundView = new UIView{BackgroundColor = UIColor.Green};

			ContentView.Layer.BorderColor = UIColor.LightGray.CGColor;
			ContentView.Layer.BorderWidth = 2.0f;
			ContentView.BackgroundColor = UIColor.White;
			ContentView.Transform = CGAffineTransform.MakeScale (0.8f, 0.8f);

			headingLabel.Text = "hi";
			/*imageView = new UIImageView (UIImage.FromBundle ("placeholder.png"));
			imageView.Center = ContentView.Center;
			imageView.Transform = CGAffineTransform.MakeScale (0.7f, 0.7f);*/

			ContentView.AddSubview (headingLabel);
		}
	}
}

