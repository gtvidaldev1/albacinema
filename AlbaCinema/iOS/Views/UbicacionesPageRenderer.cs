﻿using System;
using Xamarin.Forms;
using AlbaCinema.iOS;

[assembly:ExportRenderer (typeof(AlbaCinema.Ubicaciones2Page), typeof(UbicacionesPageRenderer))]
namespace AlbaCinema.iOS
{
	public class UbicacionesPageRenderer : TablePageRenderer
	{
		GetCinemaListAllAsyncTable[] tableItems;

		public UbicacionesPageRenderer ()
		{
			
		}

		public async override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			tableItems = await Cines2Page.Datos();
			table.Source = new UbicacionesSource(tableItems);

			Add (table);
		}
	}
}

