﻿using System;
using Xamarin.Forms;
using AlbaCinema.iOS;
using UIKit;

[assembly:ExportRenderer (typeof(AlbaCinema.AcercaPage), typeof(AcercaPageRender))]
namespace AlbaCinema.iOS
{
	public class AcercaPageRender : BasePageRenderer
	{
		public AcercaPageRender ()
		{
		}
	}
}

