﻿using System;
using UIKit;

namespace AlbaCinema.iOS
{
	public class TablePageRenderer : BasePageRenderer
	{
		protected UIKit.UITableView table;

		public TablePageRenderer ()
		{
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			//string[] tableItems = new string[] {"Vegetables","Fruits","Flower Buds","Legumes","Bulbs","Tubers"};
			table = new UITableView(View.Bounds); // defaults to Plain style
			table.BackgroundColor = UIColor.Clear;
			table.SeparatorStyle = UITableViewCellSeparatorStyle.None;
		}
	}
}

