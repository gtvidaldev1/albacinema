﻿using System;
using AlbaCinema.iOS;
using Xamarin.Forms;
using UIKit;

[assembly:ExportRenderer (typeof(AlbaCinema.Pelicula2Page), typeof(PeliculaPageRenderer))]
namespace AlbaCinema.iOS
{
	public class PeliculaPageRenderer: BasePageRenderer
	{
		protected UIImageView foto;
		protected UILabel titulo;
		protected UITextView sinopsis;
		protected UITableView table;
		TuplaClass[] tableItems;

		public PeliculaPageRenderer ()
		{
		}

		public async override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			table = new UITableView(new CoreGraphics.CGRect(0, 100, View.Bounds.Width, View.Bounds.Height - 100)); // defaults to Plain style
			table.BackgroundColor = UIColor.Clear;
			table.SeparatorStyle = UITableViewCellSeparatorStyle.None;

			tableItems = await Pelicula2Page.Datos();

			foto = new UIImageView () {
				Image = UIImage.FromBundle("vidal.jpg")
			};

			titulo = new UILabel () {
				Text = tableItems[0].Valor,
				BackgroundColor = UIColor.Clear,
				TextAlignment = UITextAlignment.Center,
				TextColor = UIColor.White
			};

			sinopsis = new UITextView (new CoreGraphics.CGRect(0, 0, View.Bounds.Width, 100)) {
				Text = tableItems[1].Valor,
				BackgroundColor = UIColor.Clear,
				Editable = false,
				TextColor = UIColor.White
			};

			table.Source = new PeliculaSource(tableItems);

			nfloat alto = sinopsis.ContentSize.Height;

			foto.Frame = new CoreGraphics.CGRect (0, 0, View.Bounds.Width / 3, 100);
			//titulo.Frame = new CoreGraphics.CGRect (View.Bounds.Width / 3, 0, View.Bounds.Width * 2 / 3, 50);
			titulo.Frame = new CoreGraphics.CGRect (0, 0, View.Bounds.Width, 50);
			sinopsis.Frame = new CoreGraphics.CGRect (0, 50, View.Bounds.Width, alto);
			table.Frame = new CoreGraphics.CGRect(0, 50 + alto, View.Bounds.Width, View.Bounds.Height - 50 - alto);

			//Add (foto);
			Add (titulo);
			Add (sinopsis);
			Add (table);
		}
	}
}

