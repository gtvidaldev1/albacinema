﻿using System;
using Xamarin.Forms;
using AlbaCinema.iOS;

[assembly:ExportRenderer (typeof(AlbaCinema.Horarios2Page), typeof(HorariosPageRenderer))]
namespace AlbaCinema.iOS
{
	public class HorariosPageRenderer : TablePageRenderer
	{
		GetMovieShowtimesAsyncTable[] tableItems;

		public HorariosPageRenderer ()
		{
		}

		public async override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			tableItems = await Horarios2Page.Datos();
			table.Source = new HorariosSource(tableItems);

			Add (table);
		}
	}
}

