﻿using System;
using UIKit;
using Xamarin.Forms;
using AlbaCinema.iOS;

[assembly:ExportRenderer (typeof(AlbaCinema.Peliculas2Page), typeof(PeliculasPageRenderer))]
namespace AlbaCinema.iOS
{
	public class PeliculasPageRenderer : TablePageRenderer
	{
		PeliculasClass[] tableItems;

		public PeliculasPageRenderer ()
		{
		}

		public async override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			tableItems = await Peliculas2Page.Datos();
			table.Source = new PeliculasSource(tableItems);

			Add (table);
		}
	}
}

