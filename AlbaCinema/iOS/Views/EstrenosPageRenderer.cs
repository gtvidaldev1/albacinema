﻿using System;
using Xamarin.Forms;
using AlbaCinema.iOS;

[assembly:ExportRenderer (typeof(AlbaCinema.Estrenos2Page), typeof(EstrenosPageRenderer))]
namespace AlbaCinema.iOS
{
	public class EstrenosPageRenderer: TablePageRenderer
	{
		PeliculasClass[] tableItems;

		public EstrenosPageRenderer ()
		{
			
		}

		public async override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			tableItems = await Estrenos2Page.Datos();
			table.Source = new EstrenosSource(tableItems);

			Add (table);
		}
	}
}

