﻿using System;

using UIKit;
using Xamarin.Forms.Platform.iOS;
using Stripe;
using CoreGraphics;
using Xamarin.Forms;
using Xamarin.Themes.Core;

[assembly:ExportRenderer (typeof(AlbaCinema.PagoPage), typeof(AlbaCinema.iOS.PagoPageRenderer))]
namespace AlbaCinema.iOS
{
	public partial class PagoPageRenderer : BasePageRenderer
	{
		StripeView stripeView;
		UIButton buttonPay;
		UITextView txtNombre;

		public PagoPageRenderer () : base ()
		{
			
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			// Perform any additional setup after loading the view, typically from a nib.

			View.BackgroundColor = UIColor.White;

			stripeView = new StripeView ();
			stripeView.Frame = new CGRect (10, 100, stripeView.Frame.Width, stripeView.Frame.Height);
			stripeView.TintColor = UIColor.White;

			txtNombre = new UITextView ();
			txtNombre.Frame = new CGRect (10, 100, stripeView.Frame.Width, stripeView.Frame.Height);

			buttonPay = new UIButton (UIButtonType.RoundedRect) {
				Frame = new CGRect (0, 160, View.Frame.Width, 50)
			};
			buttonPay.SetTitle ("Pagar Ahora", UIControlState.Normal);
			buttonPay.TouchUpInside += async delegate {

				Card card = null;

				try {
					card = stripeView.Card;

					var av = new UIAlertView ("Tarjeta", null, null, "Ok");
					
					if (!card.IsCardValid) {
						av.Message = "La tarjeta no es válida.";
					}
					else if(!card.IsNumberValid)
					{
						av.Message = "El número no es válido.";
					}
					else if (!card.IsValidCvc)
					{
						av.Message = "El CVC no es válido.";
					}
					else if (!card.IsValidExpiryDate)
					{
						av.Message = "La fecha de expiración no es valída.";
					}
					else
					{
						av.Message = "la tarjeta " + card.Type + " es correcta";
					}

					av.Show ();


				} catch (Exception ex) {
					var av = new UIAlertView ("Card Error", ex.Message, null, "OK");
					av.Show ();
					return;
				}

				/*var msg = string.Empty;

				try {
					var token = await StripeClient.CreateToken (card);

					msg = string.Format ("Good news! Stripe turned your credit card into a token: \n{0} \n\nYou can follow the instructions in the README to set up Parse as an example backend, or use this token to manually create charges at dashboard.stripe.com .", 
						token.Id);

				} catch (Exception ex) {
					msg = "Error: " + ex.Message;
				}

				var av2 = new UIAlertView ("Token Result", msg, null, "OK");
				av2.Show ();
				*/
			};

			View.AddSubview (stripeView);
			//View.AddSubview (buttonPay);
			//View.AddSubview(txtNombre);
		}

		public override void DidReceiveMemoryWarning ()
		{
			base.DidReceiveMemoryWarning ();
			// Release any cached data, images, etc that aren't in use.
		
		}
	}
}


