﻿using System;
using Xamarin.Forms.Platform.iOS;
using Xamarin.Themes.Core;

namespace AlbaCinema.iOS
{
	public class BasePageRenderer : PageRenderer
	{
		public BasePageRenderer ()
		{

		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);

			//View is a UIView
			ThemeManager.Current.Apply(View);
		}
	}
}

