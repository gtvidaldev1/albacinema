﻿using System;
using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms;
using Xamarin.Themes.Core;
using UIKit;
using AlbaCinema.iOS;
using Foundation;

[assembly:ExportRenderer (typeof(AlbaCinema.Cines2Page), typeof(CinesPageRenderer))]
namespace AlbaCinema.iOS
{
	public class CinesPageRenderer : TablePageRenderer
	{
		GetCinemaListAllAsyncTable[] tableItems;

		public CinesPageRenderer ()
		{
		}

		public async override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			tableItems = await Cines2Page.Datos();
			table.Source = new CinesSource(tableItems);

			Add (table);
		}
	}
}

