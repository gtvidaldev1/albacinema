﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Vista;

namespace AlbaCinema.iOS
{
	public class TicketingService : ITicketingService
	{
		wsTicketing.TicketingService dataService;

		public TicketingService ()
		{
			ServicePointManager.ServerCertificateValidationCallback = delegate {
				return true;
			};

			dataService = new wsTicketing.TicketingService (Constants.TicketingUrl);
		}

		public async Task<AddConcessionsResponse> 	AddConcessions (
			AddConcessionsRequest addConcessionsRequest
		)
		{
			string json;

			json = Newtonsoft.Json.JsonConvert.SerializeObject (addConcessionsRequest);

			var request = Newtonsoft.Json.JsonConvert.DeserializeObject<wsTicketing.AddConcessionsRequest> (json);

			var response = dataService.AddConcessions (request);

			json = Newtonsoft.Json.JsonConvert.SerializeObject (response);

			return Newtonsoft.Json.JsonConvert.DeserializeObject<AddConcessionsResponse> (json);
		}

		public async Task<AddSeasonPassTicketsResponse> AddSeasonPassTickets (
			AddSeasonPassTicketsRequest addSeasonPassTicketsRequest
		)
		{
			string json;

			json = Newtonsoft.Json.JsonConvert.SerializeObject (addSeasonPassTicketsRequest);

			var request = Newtonsoft.Json.JsonConvert.DeserializeObject<wsTicketing.AddSeasonPassTicketsRequest> (json);

			var response = dataService.AddSeasonPassTickets (request);

			json = Newtonsoft.Json.JsonConvert.SerializeObject (response);

			return Newtonsoft.Json.JsonConvert.DeserializeObject<AddSeasonPassTicketsResponse> (json);
		}


		public async Task<AddTicketsResponse> AddTickets (
			AddTicketsRequest addTicketsRequest
		)
		{
			string json;

			json = Newtonsoft.Json.JsonConvert.SerializeObject (addTicketsRequest);

			var request = Newtonsoft.Json.JsonConvert.DeserializeObject<wsTicketing.AddTicketsRequest> (json);

			var response = dataService.AddTickets (request);

			json = Newtonsoft.Json.JsonConvert.SerializeObject (response);

			return Newtonsoft.Json.JsonConvert.DeserializeObject<AddTicketsResponse> (json);
		}

		public async Task<ApplyDiscountsResponse> ApplyDiscounts (
			ApplyDiscountsRequest selectItemDiscountsRequest
		)
		{
			string json;

			json = Newtonsoft.Json.JsonConvert.SerializeObject (selectItemDiscountsRequest);

			var request = Newtonsoft.Json.JsonConvert.DeserializeObject<wsTicketing.ApplyDiscountsRequest> (json);

			var response = dataService.ApplyDiscounts (request);

			json = Newtonsoft.Json.JsonConvert.SerializeObject (response);

			return Newtonsoft.Json.JsonConvert.DeserializeObject<ApplyDiscountsResponse> (json);
		}

		public async Task<Response> CancelOrder (
			CancelOrderRequest cancelOrderRequest
		)
		{
			string json;

			json = Newtonsoft.Json.JsonConvert.SerializeObject (cancelOrderRequest);

			var request = Newtonsoft.Json.JsonConvert.DeserializeObject<wsTicketing.CancelOrderRequest> (json);

			var response = dataService.CancelOrder (request);

			json = Newtonsoft.Json.JsonConvert.SerializeObject (response);

			return Newtonsoft.Json.JsonConvert.DeserializeObject<Response> (json);
		}


		public async Task<CompleteOrderResponse> CompleteOrder (
			CompleteOrderRequest completeOrderRequest
		)
		{
			string json;

			json = Newtonsoft.Json.JsonConvert.SerializeObject (completeOrderRequest);

			var request = Newtonsoft.Json.JsonConvert.DeserializeObject<wsTicketing.CompleteOrderRequest> (json);

			var response = dataService.CompleteOrder (request);

			json = Newtonsoft.Json.JsonConvert.SerializeObject (response);

			return Newtonsoft.Json.JsonConvert.DeserializeObject<CompleteOrderResponse> (json);
		}

		public async Task<ExternalPaymentStartingResponse> ExternalPaymentStarting (
			ExternalPaymentStartingRequest externalPaymentStartingRequest
		)
		{
			string json;

			json = Newtonsoft.Json.JsonConvert.SerializeObject (externalPaymentStartingRequest);

			var request = Newtonsoft.Json.JsonConvert.DeserializeObject<wsTicketing.ExternalPaymentStartingRequest> (json);

			var response = dataService.ExternalPaymentStarting (request);

			json = Newtonsoft.Json.JsonConvert.SerializeObject (response);

			return Newtonsoft.Json.JsonConvert.DeserializeObject<ExternalPaymentStartingResponse> (json);
		}

		public async Task<GetCardWalletResponse> GetCardWallet (
			GetCardWalletRequest getCardWalletRequest
		)
		{
			string json;

			json = Newtonsoft.Json.JsonConvert.SerializeObject (getCardWalletRequest);

			var request = Newtonsoft.Json.JsonConvert.DeserializeObject<wsTicketing.GetCardWalletRequest> (json);

			var response = dataService.GetCardWallet (request);

			json = Newtonsoft.Json.JsonConvert.SerializeObject (response);

			return Newtonsoft.Json.JsonConvert.DeserializeObject<GetCardWalletResponse> (json);
		}

		public async Task<GetOrderResponse> GetOrder (
			GetOrderRequest getOrderRequest
		)
		{
			string json;

			json = Newtonsoft.Json.JsonConvert.SerializeObject (getOrderRequest);

			var request = Newtonsoft.Json.JsonConvert.DeserializeObject<wsTicketing.GetOrderRequest> (json);

			var response = dataService.GetOrder (request);

			json = Newtonsoft.Json.JsonConvert.SerializeObject (response);

			return Newtonsoft.Json.JsonConvert.DeserializeObject<GetOrderResponse> (json);		
		}

		public async Task<GetSessionSeatDataResponse> GetSessionSeatData (
			GetSessionSeatDataRequest getSessionSeatDataRequest
		)
		{
			string json;

			json = Newtonsoft.Json.JsonConvert.SerializeObject (getSessionSeatDataRequest);

			var request = Newtonsoft.Json.JsonConvert.DeserializeObject<wsTicketing.GetSessionSeatDataRequest> (json);

			var response = dataService.GetSessionSeatData (request);

			json = Newtonsoft.Json.JsonConvert.SerializeObject (response);

			return Newtonsoft.Json.JsonConvert.DeserializeObject<GetSessionSeatDataResponse> (json);
		}

		public async Task<OrderPaymentResponse> OrderPayment (
			OrderPaymentRequest orderPaymentRequest
		)
		{
			string json;

			json = Newtonsoft.Json.JsonConvert.SerializeObject (orderPaymentRequest);

			var request = Newtonsoft.Json.JsonConvert.DeserializeObject<wsTicketing.OrderPaymentRequest> (json);

			var response = dataService.OrderPayment (request);

			json = Newtonsoft.Json.JsonConvert.SerializeObject (response);

			return Newtonsoft.Json.JsonConvert.DeserializeObject<OrderPaymentResponse> (json);
		}

		public async Task<QueryOrderBookingStatusResponse> QueryOrderBookingStatus (
			QueryOrderBookingStatusRequest queryOrderBookingStatusRequest
		)
		{
			string json;

			json = Newtonsoft.Json.JsonConvert.SerializeObject (queryOrderBookingStatusRequest);

			var request = Newtonsoft.Json.JsonConvert.DeserializeObject<wsTicketing.QueryOrderBookingStatusRequest> (json);

			var response = dataService.QueryOrderBookingStatus (request);

			json = Newtonsoft.Json.JsonConvert.SerializeObject (response);

			return Newtonsoft.Json.JsonConvert.DeserializeObject<QueryOrderBookingStatusResponse> (json);
		}

		public async Task<RemoveCardFromWalletResponse> RemoveCardFromWallet (
			RemoveCardFromWalletRequest removeCardFromWalletRequest
		)
		{
			string json;

			json = Newtonsoft.Json.JsonConvert.SerializeObject (removeCardFromWalletRequest);

			var request = Newtonsoft.Json.JsonConvert.DeserializeObject<wsTicketing.RemoveCardFromWalletRequest> (json);

			var response = dataService.RemoveCardFromWallet (request);

			json = Newtonsoft.Json.JsonConvert.SerializeObject (response);

			return Newtonsoft.Json.JsonConvert.DeserializeObject<RemoveCardFromWalletResponse> (json);
		}

		public async Task<RemoveConcessionsResponse> RemoveConcessions (
			RemoveConcessionsRequest removeConcessionsRequest
		)
		{
			string json;

			json = Newtonsoft.Json.JsonConvert.SerializeObject (removeConcessionsRequest);

			var request = Newtonsoft.Json.JsonConvert.DeserializeObject<wsTicketing.RemoveConcessionsRequest> (json);

			var response = dataService.RemoveConcessions (request);

			json = Newtonsoft.Json.JsonConvert.SerializeObject (response);

			return Newtonsoft.Json.JsonConvert.DeserializeObject<RemoveConcessionsResponse> (json);
		}

		public async Task<RemoveTicketsResponse> RemoveTickets (
			RemoveTicketsRequest removeTicketsRequest
		)
		{
			string json;

			json = Newtonsoft.Json.JsonConvert.SerializeObject (removeTicketsRequest);

			var request = Newtonsoft.Json.JsonConvert.DeserializeObject<wsTicketing.RemoveTicketsRequest> (json);

			var response = dataService.RemoveTickets (request);

			json = Newtonsoft.Json.JsonConvert.SerializeObject (response);

			return Newtonsoft.Json.JsonConvert.DeserializeObject<RemoveTicketsResponse> (json);
		}

		public async Task<ResetOrderExpiryResponse> ResetOrderExpiry (
			ResetOrderExpiryRequest resetOrderExpiryRequest
		)
		{
			string json;

			json = Newtonsoft.Json.JsonConvert.SerializeObject (resetOrderExpiryRequest);

			var request = Newtonsoft.Json.JsonConvert.DeserializeObject<wsTicketing.ResetOrderExpiryRequest> (json);

			var response = dataService.ResetOrderExpiry (request);

			json = Newtonsoft.Json.JsonConvert.SerializeObject (response);

			return Newtonsoft.Json.JsonConvert.DeserializeObject<ResetOrderExpiryResponse> (json);
		}

		public async Task<SetSelectedSeatsResponse> SetSelectedSeats (
			SetSelectedSeatsRequest setSelectedSeatsRequest
		)
		{
			string json;

			json = Newtonsoft.Json.JsonConvert.SerializeObject (setSelectedSeatsRequest);

			var request = Newtonsoft.Json.JsonConvert.DeserializeObject<wsTicketing.SetSelectedSeatsRequest> (json);

			var response = dataService.SetSelectedSeats (request);

			json = Newtonsoft.Json.JsonConvert.SerializeObject (response);

			return Newtonsoft.Json.JsonConvert.DeserializeObject<SetSelectedSeatsResponse> (json);
		}

		public async Task<ValidateMemberTicketResponse> ValidateMemberTickets (
			ValidateMemberTicketRequest validateMemberTicketRequest
		)
		{
			string json;

			json = Newtonsoft.Json.JsonConvert.SerializeObject (validateMemberTicketRequest);

			var request = Newtonsoft.Json.JsonConvert.DeserializeObject<wsTicketing.ValidateMemberTicketRequest> (json);

			var response = dataService.ValidateMemberTickets (request);

			json = Newtonsoft.Json.JsonConvert.SerializeObject (response);

			return Newtonsoft.Json.JsonConvert.DeserializeObject<ValidateMemberTicketResponse> (json);
		}
	}
}

