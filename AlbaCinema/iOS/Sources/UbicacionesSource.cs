﻿using System;
using System.Linq;
using UIKit;
using Foundation;

namespace AlbaCinema.iOS
{
	public class UbicacionesSource: CinesSource
	{
		public UbicacionesSource (GetCinemaListAllAsyncTable[] items) : base(items)
		{
		}

		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			UITableViewCell cell = tableView.DequeueReusableCell (CellIdentifier);
			GetCinemaListAllAsyncTable item = TableItems [indexPath.Row];

			//---- if there are no cells to reuse, create a new one
			if (cell == null) {
				cell = new UITableViewCell (UITableViewCellStyle.Subtitle, CellIdentifier);
			}

			var query = from a in AlbaContext.Cines ()
			            where a.Cinema_strID == item.Cinema_strID
			            select a;

			cell.TextLabel.Text = item.Cinema_strName.TrimStart ();

			if (query.Count () > 0) {
				string Direccion = item.Cinema_strAddress1  + ", " + item.Cinema_strAddress2;

				if (Direccion.Trim () != ",") {

					cell.DetailTextLabel.Text = Direccion;
				} else {

					cell.DetailTextLabel.Text = "Teléfono: " + query.First().Cinema_strPhoneCountryCode.Substring(0,4)
						+ "-" + query.First ().Cinema_strPhoneCountryCode.Substring (4);
				}

				// Disclosure es chevron
				cell.Accessory = UITableViewCellAccessory.DetailDisclosureButton;
				/*UIButton phoneIndicator = new UIButton (UIButtonType.DetailDisclosure);
				phoneIndicator.SetImage ( UIImage.FromBundle ("phone.png"), UIControlState.Normal);
				cell.AccessoryView = phoneIndicator;*/
			} else {
				cell.DetailTextLabel.Text = "Teléfono no disponible";
				// Disclosure es chevron
				cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;
			}
			cell.BackgroundColor = UIColor.Clear;
			cell.TextLabel.TextColor = UIColor.White;
			cell.DetailTextLabel.TextColor = UIColor.LightTextColor;

			return cell;
		}


		/// <summary>
		/// Called when the DetailDisclosureButton is touched.
		/// Does nothing if DetailDisclosureButton isn't in the cell
		/// </summary>
		public override void AccessoryButtonTapped (UITableView tableView, NSIndexPath indexPath)
		{
			Ubicaciones2Page.Llamar (TableItems [indexPath.Row]);
		}

		/// <summary>
		/// Called when a row is touched
		/// </summary>
		public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
		{
			Ubicaciones2Page.Navegar (TableItems [indexPath.Row]);
		}
	}
}

