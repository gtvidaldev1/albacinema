﻿using System;
using UIKit;
using Foundation;

namespace AlbaCinema.iOS
{
	public class HorariosSource:UITableViewSource
	{
		protected GetMovieShowtimesAsyncTable[] TableItems;
		protected string CellIdentifier = "TableCell";

		public HorariosSource (GetMovieShowtimesAsyncTable[] items)
		{
			TableItems = items;
		}

		public override nint RowsInSection (UITableView tableview, nint section)
		{
			return TableItems.Length;
		}

		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			UITableViewCell cell = tableView.DequeueReusableCell (CellIdentifier);
			GetMovieShowtimesAsyncTable item = TableItems [indexPath.Row];

			//---- if there are no cells to reuse, create a new one
			if (cell == null) {
				cell = new UITableViewCell (UITableViewCellStyle.Subtitle, CellIdentifier);
			}

			cell.TextLabel.Text = item.Session_dtmDate_Time.ToString("HH:mm:ss tt");
			cell.DetailTextLabel.Text = item.Screen_strName;
			cell.BackgroundColor = UIColor.Clear;
			cell.TextLabel.TextColor = UIColor.White;
			// Disclosure es chevron
			cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;
			cell.DetailTextLabel.TextColor = UIColor.LightTextColor;

			return cell;
		}

		/// <summary>
		/// Called when the DetailDisclosureButton is touched.
		/// Does nothing if DetailDisclosureButton isn't in the cell
		/// </summary>
		public override void AccessoryButtonTapped (UITableView tableView, NSIndexPath indexPath)
		{
			new UIAlertView("Advertencia"
				, TableItems[indexPath.Row].Cinema_strName + " aún no acepta compras en línea.", null, "OK", null).Show();
		}

		/// <summary>
		/// Called when a row is touched
		/// </summary>
		public async override void RowSelected (UITableView tableView, NSIndexPath indexPath)
		{
			await Horarios2Page.Navegar (TableItems [indexPath.Row]);
		}
	}
}

