﻿using System;
using UIKit;
using Foundation;

namespace AlbaCinema.iOS
{
	public class PeliculaSource:UITableViewSource
	{
		protected TuplaClass[] TableItems;
		protected string CellIdentifier = "TableCell";

		public PeliculaSource (TuplaClass[] items)
		{
			TableItems = items;
		}

		public override nint RowsInSection (UITableView tableview, nint section)
		{
			return TableItems.Length;
		}

		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			UITableViewCell cell = tableView.DequeueReusableCell (CellIdentifier);
			TuplaClass item = TableItems [indexPath.Row];

			//---- if there are no cells to reuse, create a new one
			if (cell == null) {
				cell = new UITableViewCell (UITableViewCellStyle.Value1, CellIdentifier);
			}

			cell.TextLabel.Text = item.Nombre;
			cell.DetailTextLabel.Text = item.Valor;
			cell.BackgroundColor = UIColor.Clear;
			cell.TextLabel.TextColor = UIColor.White;
			cell.DetailTextLabel.TextColor = UIColor.LightTextColor;

			return cell;
		}

		/// <summary>
		/// Called when the DetailDisclosureButton is touched.
		/// Does nothing if DetailDisclosureButton isn't in the cell
		/// </summary>
		public override void AccessoryButtonTapped (UITableView tableView, NSIndexPath indexPath)
		{
			new UIAlertView("Advertencia"
				, TableItems[indexPath.Row].Nombre + " aún no acepta compras en línea.", null, "OK", null).Show();
		}

		/// <summary>
		/// Called when a row is touched
		/// </summary>
		public async override void RowSelected (UITableView tableView, NSIndexPath indexPath)
		{
			//await Cines2Page.Navegar (TableItems [indexPath.Row]);
		}
	}
}

