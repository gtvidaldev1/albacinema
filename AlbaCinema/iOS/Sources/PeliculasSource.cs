﻿using System;
using UIKit;
using Foundation;
using System.Threading.Tasks;

namespace AlbaCinema.iOS
{
	public class PeliculasSource:BaseSource
	{
		protected PeliculasClass[] TableItems;

		public PeliculasSource (PeliculasClass[] items)
		{
			TableItems = items;
		}

		public override nint RowsInSection (UITableView tableview, nint section)
		{
			return TableItems.Length;
		}

		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			UITableViewCell cell = tableView.DequeueReusableCell (CellIdentifier);
			PeliculasClass item = TableItems [indexPath.Row];

			//---- if there are no cells to reuse, create a new one
			if (cell == null) {
				cell =  new UITableViewCell (UITableViewCellStyle.Subtitle, CellIdentifier);
			}
			
			//cell.UpdateCell (item.Movie_strName, item.Movie_strRating, foto);
			cell.TextLabel.Text = item.Movie_strName;
			cell.DetailTextLabel.Text = item.Movie_strRating;
			cell.BackgroundColor = UIColor.Clear;
			cell.TextLabel.TextColor = UIColor.White;

			LoadImage (TableItems [indexPath.Row].Film_strURLforFilmName).ContinueWith((task) => InvokeOnMainThread(() =>
				{
					cell.ImageView.Image = task.Result;
				}));

			// Disclosure es chevron
			cell.Accessory = UITableViewCellAccessory.DetailDisclosureButton;
			cell.DetailTextLabel.TextColor = UIColor.LightTextColor;

			LoadImage (TableItems [indexPath.Row].Film_strURLforFilmName).ContinueWith((task) => InvokeOnMainThread(() =>
				{
					cell.ImageView.Image = task.Result;
				}));
			
			return cell;
		}

		/// <summary>
		/// Called when the DetailDisclosureButton is touched.
		/// Does nothing if DetailDisclosureButton isn't in the cell
		/// </summary>
		public async override void AccessoryButtonTapped (UITableView tableView, NSIndexPath indexPath)
		{
			await Peliculas2Page.Informacion (TableItems [indexPath.Row]);
		}

		/// <summary>
		/// Called when a row is touched
		/// </summary>
		public async override void RowSelected (UITableView tableView, NSIndexPath indexPath)
		{
			await Peliculas2Page.Navegar (TableItems [indexPath.Row]);
		}
	}
}

