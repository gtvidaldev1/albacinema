﻿using System;
using UIKit;
using Foundation;

namespace AlbaCinema.iOS
{
	public class CinesSource:UITableViewSource
	{
		protected GetCinemaListAllAsyncTable[] TableItems;
		protected string CellIdentifier = "TableCell";

		public CinesSource (GetCinemaListAllAsyncTable[] items)
		{
			TableItems = items;
		}

		public override nint RowsInSection (UITableView tableview, nint section)
		{
			return TableItems.Length;
		}

		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			UITableViewCell cell = tableView.DequeueReusableCell (CellIdentifier);
			GetCinemaListAllAsyncTable item = TableItems [indexPath.Row];

			//---- if there are no cells to reuse, create a new one
			if (cell == null) {
				cell = new UITableViewCell (UITableViewCellStyle.Default, CellIdentifier);
			}

			cell.TextLabel.Text = item.Cinema_strName.TrimStart ();
			cell.BackgroundColor = UIColor.Clear;
			cell.TextLabel.TextColor = UIColor.White;
			// Disclosure es chevron
			if (Cines2Page.CompraEnLinea (item)) {
				cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;
			} else {
				cell.Accessory = UITableViewCellAccessory.DetailDisclosureButton;
			}

			return cell;
		}

		/// <summary>
		/// Called when the DetailDisclosureButton is touched.
		/// Does nothing if DetailDisclosureButton isn't in the cell
		/// </summary>
		public override void AccessoryButtonTapped (UITableView tableView, NSIndexPath indexPath)
		{
			new UIAlertView ("Advertencia"
				, TableItems [indexPath.Row].Cinema_strName + " aún no acepta compras en línea.", null, "OK", null).Show ();
		}

		/// <summary>
		/// Called when a row is touched
		/// </summary>
		public override async void RowSelected (UITableView tableView, NSIndexPath indexPath)
		{
			await Cines2Page.Navegar (TableItems [indexPath.Row]);
		}
	}
}

