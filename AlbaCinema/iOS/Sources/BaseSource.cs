﻿using System;
using UIKit;
using Foundation;
using System.Threading.Tasks;
using System.Net.Http;

namespace AlbaCinema.iOS
{
	public class BaseSource:UITableViewSource
	{
		protected string CellIdentifier = "TableCell";

		public override nint RowsInSection (UITableView tableview, nint section)
		{
			return 0;
		}

		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			UITableViewCell cell = tableView.DequeueReusableCell (CellIdentifier);

			//---- if there are no cells to reuse, create a new one
			if (cell == null) {
				cell = new UITableViewCell (UITableViewCellStyle.Default, CellIdentifier);
			}

			return cell;
		}
		protected UIImage FromUrl (string uri)
		{
			using (var url = new NSUrl (uri))
			using (var data = NSData.FromUrl (url))
				if (data != null) {
					return UIImage.LoadFromData (data);
				} else {
					return null;
				}
		}

		public async Task<UIImage> LoadImage (string imageUrl)
		{
			var httpClient = new HttpClient();

			try
			{
			Task<byte[]> contentsTask = httpClient.GetByteArrayAsync (imageUrl);

			// await! control returns to the caller and the task continues to run on another thread
			var contents = await contentsTask;

			// load from bytes
			return UIImage.LoadFromData (NSData.FromArray (contents));
			}
			catch(Exception) {
				return null;
			}
		}
	}
}

