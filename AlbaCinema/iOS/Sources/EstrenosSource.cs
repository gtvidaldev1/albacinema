﻿using System;
using UIKit;
using Foundation;

namespace AlbaCinema.iOS
{
	public class EstrenosSource : PeliculasSource
	{
		public EstrenosSource (PeliculasClass[] items) : base(items)
		{
		}

		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			UITableViewCell cell = tableView.DequeueReusableCell (CellIdentifier);
			PeliculasClass item = TableItems [indexPath.Row];

			//---- if there are no cells to reuse, create a new one
			if (cell == null) {
				cell =  new UITableViewCell (UITableViewCellStyle.Subtitle, CellIdentifier);
			}
					
			cell.TextLabel.Text = item.Movie_strName;
			cell.DetailTextLabel.Text = item.Movie_strRating;
			cell.BackgroundColor = UIColor.Clear;
			cell.TextLabel.TextColor = UIColor.White;

			LoadImage (TableItems [indexPath.Row].Film_strURLforFilmName).ContinueWith((task) => InvokeOnMainThread(() =>
				{
					cell.ImageView.Image = task.Result;
				}));

			// Disclosure es chevron
			cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;
			cell.DetailTextLabel.TextColor = UIColor.LightTextColor;

			return cell;
		}

		/// <summary>
		/// Called when a row is touched
		/// </summary>
		public async override void RowSelected (UITableView tableView, NSIndexPath indexPath)
		{
			await Estrenos2Page.Informacion (TableItems [indexPath.Row]);
		}
	}
}

