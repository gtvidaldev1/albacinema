﻿using System;
using Xamarin.Themes.TrackBeam;
using UIKit;

namespace AlbaCinema.iOS
{
	public class CustomTheme: TrackBeamTheme
	{
		/*public override UIColor BaseTintColor
		{
			get
			{
				return UIColor.Red;
			}
		}*/

		public override string PhoneBackgroundName {
			get
			{
				return "abyss.jpg";
			}
		}

		public override string PhoneBackgroundLandscapeName {
			get
			{
				return "abyssh.jpg";
			}
		}

		public override string PadBackgroundName {
			get
			{
				return "abyss_ipad.jpg";
			}
		}

		public override string PadBackgroundLandscapeName {
			get
			{
				return "abyss_ipad.jpg";
			}
		}
		/*
		public override string ToolbarBackgroundName {
			get;
		}

		public override string ToolbarLandscapeBackgroundName {
			get;
		}

		public override string NavigationBarBackgroundName {
			get;
		}

		public override string NavigationBarLandscapeBackgroundName {
			get;
		}

		public override string NavigationBarRightBackgroundName {
			get;
		}

		public override string NavigationBarRightLandscapeBackgroundName {
			get;
		}

		public override string TabbarBackgroundName {
			get;
		}*/
	}
}

