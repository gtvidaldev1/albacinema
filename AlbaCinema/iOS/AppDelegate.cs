﻿using System;
using System.Collections.Generic;
using System.Linq;
using Foundation;
using UIKit;
using Xamarin.Themes;
using Xamarin.Themes.Core;
using Xamarin.Themes.TrackBeam;
using Xamarin.Forms;

namespace AlbaCinema.iOS
{
	[Register ("AppDelegate")]
	public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
	{
		public override bool FinishedLaunching (UIApplication app, NSDictionary options)
		{
			global::Xamarin.Forms.Forms.Init ();

			App.AlbaManager = new DataManager (new DataService ());
			App.TicketingManager = new TicketingManager (new TicketingService ()); 

			LoadApplication (new App ());

			bool finished = base.FinishedLaunching (app, options);

			ThemeManager.Register<CustomTheme>().Apply();

			return finished;
		}

		/*public override UIInterfaceOrientationMask GetSupportedInterfaceOrientations(UIApplication application, UIWindow forWindow)
		{
			if (Xamarin.Forms.Application.Current == null || Xamarin.Forms.Application.Current.MainPage == null)
			{
				return UIInterfaceOrientationMask.Portrait;
			}

			var mainPage = Xamarin.Forms.Application.Current.MainPage;

			if (mainPage is MainPage && ((NavigationPage)((MainPage)mainPage).Detail).CurrentPage is AsientosPage) {
				return UIInterfaceOrientationMask.Landscape;
			} else {
				return UIInterfaceOrientationMask.All;
			}
				
			/*if (mainPage is MainPage ||
				(mainPage is NavigationPage && ((NavigationPage)mainPage).CurrentPage is AsientosPage) ||
				(mainPage.Navigation != null && mainPage.Navigation.ModalStack.LastOrDefault() is AsientosPage))
			{
				return UIInterfaceOrientationMask.Landscape;
			}*/
		/*
			return UIInterfaceOrientationMask.Portrait;
		}*/
	}
}

