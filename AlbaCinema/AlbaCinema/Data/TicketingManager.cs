﻿using System;
using System.Threading.Tasks;
using Vista;

namespace AlbaCinema
{
	public class TicketingManager
	{
		ITicketingService soapService;

		public TicketingManager (ITicketingService service)
		{
			soapService = service;
		}

		public Task<AddConcessionsResponse> 	AddConcessions (
			AddConcessionsRequest addConcessionsRequest
		)
		{
			return soapService.AddConcessions (addConcessionsRequest);
		}

		public Task<AddSeasonPassTicketsResponse> AddSeasonPassTickets (
			AddSeasonPassTicketsRequest addSeasonPassTicketsRequest
		)
		{
			return soapService.AddSeasonPassTickets (addSeasonPassTicketsRequest);
		}

		public Task<AddTicketsResponse> AddTickets (
			AddTicketsRequest addTicketsRequest
		)
		{
			return soapService.AddTickets (addTicketsRequest);
		}

		public Task<ApplyDiscountsResponse> ApplyDiscounts(
			ApplyDiscountsRequest selectItemDiscountsRequest
		)
		{
			return soapService.ApplyDiscounts (selectItemDiscountsRequest);
		}

		public Task<Response> CancelOrder(
			CancelOrderRequest cancelOrderRequest
		)
		{
			return soapService.CancelOrder (cancelOrderRequest);
		}

		public Task<CompleteOrderResponse> CompleteOrder(
			CompleteOrderRequest completeOrderRequest
		)
		{
			return soapService.CompleteOrder (completeOrderRequest);
		}

		public Task<ExternalPaymentStartingResponse> ExternalPaymentStarting(
			ExternalPaymentStartingRequest externalPaymentStartingRequest
		)
		{
			return soapService.ExternalPaymentStarting (externalPaymentStartingRequest);
		}

		public Task<GetCardWalletResponse> GetCardWallet(
			GetCardWalletRequest getCardWalletRequest
		)
		{
			return soapService.GetCardWallet (getCardWalletRequest);
		}

		public Task<GetOrderResponse> GetOrder(
			GetOrderRequest getOrderRequest
		)
		{
			return soapService.GetOrder (getOrderRequest);
		}

		public Task<GetSessionSeatDataResponse> GetSessionSeatData(
			GetSessionSeatDataRequest getSessionSeatDataRequest
		)
		{
			return soapService.GetSessionSeatData (getSessionSeatDataRequest);
		}

		public Task<OrderPaymentResponse> OrderPayment(
			OrderPaymentRequest orderPaymentRequest
		)
		{
			return soapService.OrderPayment (orderPaymentRequest);
		}

		public Task<QueryOrderBookingStatusResponse> QueryOrderBookingStatus(
			QueryOrderBookingStatusRequest queryOrderBookingStatusRequest
		)
		{
			return soapService.QueryOrderBookingStatus (queryOrderBookingStatusRequest);
		}

		public Task<RemoveCardFromWalletResponse> RemoveCardFromWallet(
			RemoveCardFromWalletRequest removeCardFromWalletRequest
		)
		{
			return soapService.RemoveCardFromWallet (removeCardFromWalletRequest);
		}

		public Task<RemoveConcessionsResponse> RemoveConcessions(
			RemoveConcessionsRequest removeConcessionsRequest
		)
		{
			return soapService.RemoveConcessions (removeConcessionsRequest);
		}

		public Task<RemoveTicketsResponse> RemoveTickets(
			RemoveTicketsRequest removeTicketsRequest
		)
		{
			return soapService.RemoveTickets (removeTicketsRequest);
		}

		public Task<ResetOrderExpiryResponse> ResetOrderExpiry(
			ResetOrderExpiryRequest resetOrderExpiryRequest
		)
		{
			return soapService.ResetOrderExpiry (resetOrderExpiryRequest);
		}

		public Task<SetSelectedSeatsResponse> SetSelectedSeats(
			SetSelectedSeatsRequest setSelectedSeatsRequest
		)
		{
			return soapService.SetSelectedSeats (setSelectedSeatsRequest);
		}

		public Task<ValidateMemberTicketResponse> ValidateMemberTickets(
			ValidateMemberTicketRequest validateMemberTicketRequest
		)
		{
			return soapService.ValidateMemberTickets (validateMemberTicketRequest);
		}
	}
}

