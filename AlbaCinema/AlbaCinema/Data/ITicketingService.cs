﻿using System;
using System.Threading.Tasks;
using Vista;

namespace AlbaCinema
{
	public interface ITicketingService
	{
		Task<AddConcessionsResponse> 	AddConcessions (
			AddConcessionsRequest addConcessionsRequest
		);

		Task<AddSeasonPassTicketsResponse> AddSeasonPassTickets (
			AddSeasonPassTicketsRequest addSeasonPassTicketsRequest
		);

		Task<AddTicketsResponse> AddTickets (
			AddTicketsRequest addTicketsRequest
		);

		Task<ApplyDiscountsResponse> ApplyDiscounts(
			ApplyDiscountsRequest selectItemDiscountsRequest
		);

		Task<Response> CancelOrder(
			CancelOrderRequest cancelOrderRequest
		);

		Task<CompleteOrderResponse> CompleteOrder(
			CompleteOrderRequest completeOrderRequest
		);

		Task<ExternalPaymentStartingResponse> ExternalPaymentStarting(
			ExternalPaymentStartingRequest externalPaymentStartingRequest
		);

		Task<GetCardWalletResponse> GetCardWallet(
			GetCardWalletRequest getCardWalletRequest
		);

		Task<GetOrderResponse> GetOrder(
			GetOrderRequest getOrderRequest
		);

		Task<GetSessionSeatDataResponse> GetSessionSeatData(
			GetSessionSeatDataRequest getSessionSeatDataRequest
		);

		Task<OrderPaymentResponse> OrderPayment(
			OrderPaymentRequest orderPaymentRequest
		);

		Task<QueryOrderBookingStatusResponse> QueryOrderBookingStatus(
			QueryOrderBookingStatusRequest queryOrderBookingStatusRequest
		);

		Task<RemoveCardFromWalletResponse> RemoveCardFromWallet(
			RemoveCardFromWalletRequest removeCardFromWalletRequest
		);

		Task<RemoveConcessionsResponse> RemoveConcessions(
			RemoveConcessionsRequest removeConcessionsRequest
		);

		Task<RemoveTicketsResponse> RemoveTickets(
			RemoveTicketsRequest removeTicketsRequest
		);

		Task<ResetOrderExpiryResponse> ResetOrderExpiry(
			ResetOrderExpiryRequest resetOrderExpiryRequest
		);

		Task<SetSelectedSeatsResponse> SetSelectedSeats(
			SetSelectedSeatsRequest setSelectedSeatsRequest
		);

		Task<ValidateMemberTicketResponse> ValidateMemberTickets(
			ValidateMemberTicketRequest validateMemberTicketRequest
		);
	}
}

