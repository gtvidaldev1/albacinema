﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AlbaCinema
{
	public class AlbaContext
	{
		public AlbaContext ()
		{
		}

		public static IEnumerable<GetCinemaListAllAsyncTable> Cines ()
		{
			List<GetCinemaListAllAsyncTable> lst = new List<GetCinemaListAllAsyncTable> ();

			lst.Add (new GetCinemaListAllAsyncTable () {
				Cinema_strID = "9983",
				Cinema_decLatitude = 14.59998,
				Cinema_decLongitude = -90.51211,
				Cinema_strAddress1 = "",
				Cinema_strPhoneCountryCode = "23366858"
			});

			lst.Add (new GetCinemaListAllAsyncTable () {
				Cinema_strID = "9999",
				Cinema_decLatitude = 14.55162,
				Cinema_decLongitude = -90.4547,
				Cinema_strAddress1 = "",
				Cinema_strPhoneCountryCode = "22183251"
			});

			lst.Add (new GetCinemaListAllAsyncTable () {
				Cinema_strID = "9998",
				Cinema_decLatitude = 14.57984,
				Cinema_decLongitude = -90.49515,
				Cinema_strAddress1 = "",
				Cinema_strPhoneCountryCode = "22183276"
			});

			lst.Add (new GetCinemaListAllAsyncTable () {
				Cinema_strID = "9986",
				Cinema_decLatitude = 14.57984,
				Cinema_decLongitude = -90.49515,
				Cinema_strAddress1 = "",
				Cinema_strPhoneCountryCode = "22183256"
			});

			lst.Add (new GetCinemaListAllAsyncTable () {
				Cinema_strID = "9982",
				Cinema_decLatitude = 14.850520,
				Cinema_decLongitude = -91.534438,
				Cinema_strAddress1 = "",
				Cinema_strPhoneCountryCode = "22183260"
			});

			lst.Add (new GetCinemaListAllAsyncTable () {
				Cinema_strID = "9985",
				Cinema_decLatitude = 14.662712,
				Cinema_decLongitude = -90.810948,
				Cinema_strAddress1 = "",
				Cinema_strPhoneCountryCode = "78404723"
			});

			lst.Add (new GetCinemaListAllAsyncTable () {
				Cinema_strID = "9987",
				Cinema_decLatitude = 14.807823,
				Cinema_decLongitude = -89.533481,
				Cinema_strAddress1 = "",
				Cinema_strPhoneCountryCode = "22183253"
			});

			lst.Add (new GetCinemaListAllAsyncTable () {
				Cinema_strID = "9988",
				Cinema_decLatitude = 15.470271,
				Cinema_decLongitude = -90.385671,
				Cinema_strAddress1 = "",
				Cinema_strPhoneCountryCode = "22183252"
			});

			lst.Add (new GetCinemaListAllAsyncTable () {
				Cinema_strID = "9989",
				Cinema_decLatitude = 14.289711,
				Cinema_decLongitude = -90.785078,
				Cinema_strAddress1 = "",
				Cinema_strPhoneCountryCode = "22183255"
			});

			lst.Add (new GetCinemaListAllAsyncTable () {
				Cinema_strID = "9984",
				Cinema_decLatitude = 16.923582,
				Cinema_decLongitude = -89.890931,
				Cinema_strAddress1 = "",
				Cinema_strPhoneCountryCode = "22183258"
			});

			lst.Add (new GetCinemaListAllAsyncTable () {
				Cinema_strID = "9990",
				Cinema_decLatitude = 15.69803,
				Cinema_decLongitude = -88.58729,
				Cinema_strAddress1 = "",
				Cinema_strPhoneCountryCode = "22183259"
			});
			
			return lst;
		}
	}
}

