﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace AlbaCinema
{
	public interface IDataService
	{
		Task<DataResponse> GetCinemaListAsync (GetCinemaListRequest GetCinemaListRequest);

		Task<DataResponse> GetCinemaListAllAsync (GetCinemaListAllRequest GetCinemaListAllRequest);

		Task<DataResponse> GetMovieListAsync (GetMovieListRequest GetMovieListRequest);

		Task<DataResponse> GetMovieInfoListAsync (GetMovieInfoListRequest GetMovieInfoListRequest);

		Task<DataResponse> GetMoviePeopleAsync (GetMoviePeopleRequest GetMoviePeopleRequest);

		Task<DataResponse> GetSessionListAsync (GetSessionListRequest GetSessionListRequest);

		Task<DataResponse> GetSessionInfoAsync (GetSessionInfoRequest GetSessionInfoRequest);

		Task<DataResponse> GetTicketTypeListAsync (GetTicketTypeListRequest GetTicketTypeListRequest);

		Task<DataResponse> GetTicketTypeFromBarcodeAsync (GetTicketTypeFromBarcodeRequest GetTicketTypeFromBarcodeRequest);

		Task<DataResponse> GetTicketTypePackageAsync (GetTicketTypePackageRequest GetTicketTypePackageRequest);

		Task<DataResponse> GetEventListAsync (GetEventListRequest GetEventListRequest);

		Task<DataResponse> GetEventMovieListAsync (GetEventMovieListRequest GetEventMovieListRequest);

		Task<DataResponse> GetShowtimeDateListAsync (GetShowtimeDateListRequest GetShowtimeDateListRequest);

		Task<DataResponse> GetCinemaOpForSessionAsync (GetCinemaOpForSessionRequest GetCinemaOpForSessionRequest);

		Task<DataResponse> GetPrintStreamAsync (GetPrintStreamRequest GetPrintStreamRequest);

		Task<DataResponse> GetConfirmationDetailsAsync (GetConfirmationDetailsRequest GetConfirmationDetailsRequest);

		Task<DataResponse> GetConcessionItemsListAsync (GetConcessionItemsListRequest GetConcessionItemsListRequest);

		Task<DataResponse> GetMovieShowtimesAsync (GetMovieShowtimesRequest GetMovieShowtimesRequest);

		Task<DataResponse> GetProductInfoAsync (GetProductInfoRequest GetProductInfoRequest);

		Task<DataResponse> GetClientListAsync (GetClientListRequest GetClientListRequest);

		Task<DataResponse> GiftCardBalanceRequestAsync (GiftCardBalanceRequest GCBalanceRequest);

		Task<Response> CreateClientAsync (CreateClientRequest CreateClientRequest);

		Task<Response> UpdateClientAsync (UpdateClientRequest UpdateClientRequest);

		Task<DataResponse> GetLtyMembershipConcessionItemAsync (GetLtyMembershipConcessionItemRequest GetLtyMembershipConcessionItemRequest);

		Task<DataResponse> GetConfigSettingListAsync (GetConfigSettingListRequest GetConfigSettingListRequest);

		Task<GetOrderHistoryResponse> GetOrderHistoryAsync (GetOrderHistoryRequest GetOrderHistoryRequest);

		Task<GetCinemaSiteGroupsResponse> GetCinemaSiteGroupsAsync (GetCinemaSiteGroupsRequest GetCinemaSiteGroupsRequest);

		Task<GetConcessionPrintStreamsResponse> GetConcessionPrintStreamsAsync (GetConcessionPrintStreamsRequest GetConcessionPrintStreamsRequest);

		Task<GetSessionSeatPlanResponse> GetSessionSeatPlanAsync (GetSessionSeatPlanRequest GetSessionSeatPlanRequest);
	}
}

