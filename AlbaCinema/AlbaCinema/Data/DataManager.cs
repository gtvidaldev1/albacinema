﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace AlbaCinema
{
	public class DataManager
	{
		IDataService soapService;

		public DataManager (IDataService service)
		{
			soapService = service;
		}

		public Task<DataResponse> GetCinemaListAsync (GetCinemaListRequest GetCinemaListRequest)
		{
			return soapService.GetCinemaListAsync (GetCinemaListRequest);
		}

		public Task<DataResponse> GetCinemaListAllAsync (GetCinemaListAllRequest GetCinemaListAllRequest)
		{
			return soapService.GetCinemaListAllAsync (GetCinemaListAllRequest);
		}

		public Task<DataResponse> GetMovieListAsync (GetMovieListRequest GetMovieListRequest)
		{
			return soapService.GetMovieListAsync (GetMovieListRequest);
		}

		public Task<DataResponse> GetMovieInfoListAsync (GetMovieInfoListRequest GetMovieInfoListRequest)
		{
			return soapService.GetMovieInfoListAsync (GetMovieInfoListRequest);
		}

		public Task<DataResponse> GetMoviePeopleAsync (GetMoviePeopleRequest GetMoviePeopleRequest)
		{
			return soapService.GetMoviePeopleAsync (GetMoviePeopleRequest);
		}

		public Task<DataResponse> GetSessionListAsync (GetSessionListRequest GetSessionListRequest)
		{
			return soapService.GetSessionListAsync (GetSessionListRequest);
		}

		public Task<DataResponse> GetSessionInfoAsync (GetSessionInfoRequest GetSessionInfoRequest)
		{
			return soapService.GetSessionInfoAsync (GetSessionInfoRequest);
		}

		public Task<DataResponse> GetTicketTypeListAsync (GetTicketTypeListRequest GetTicketTypeListRequest)
		{
			return soapService.GetTicketTypeListAsync (GetTicketTypeListRequest);
		}

		public Task<DataResponse> GetTicketTypeFromBarcodeAsync (GetTicketTypeFromBarcodeRequest GetTicketTypeFromBarcodeRequest)
		{
			return soapService.GetTicketTypeFromBarcodeAsync (GetTicketTypeFromBarcodeRequest);
		}

		public Task<DataResponse> GetTicketTypePackageAsync (GetTicketTypePackageRequest GetTicketTypePackageRequest)
		{
			return soapService.GetTicketTypePackageAsync (GetTicketTypePackageRequest);
		}

		public Task<DataResponse> GetEventListAsync (GetEventListRequest GetEventListRequest)
		{
			return soapService.GetEventListAsync (GetEventListRequest);
		}

		public Task<DataResponse> GetEventMovieListAsync (GetEventMovieListRequest GetEventMovieListRequest)
		{
			return soapService.GetEventMovieListAsync (GetEventMovieListRequest);
		}

		public Task<DataResponse> GetShowtimeDateListAsync (GetShowtimeDateListRequest GetShowtimeDateListRequest)
		{
			return soapService.GetShowtimeDateListAsync (GetShowtimeDateListRequest);
		}

		public Task<DataResponse> GetCinemaOpForSessionAsync (GetCinemaOpForSessionRequest GetCinemaOpForSessionRequest)
		{
			return soapService.GetCinemaOpForSessionAsync (GetCinemaOpForSessionRequest);
		}

		public Task<DataResponse> GetPrintStreamAsync (GetPrintStreamRequest GetPrintStreamRequest)
		{
			return soapService.GetPrintStreamAsync (GetPrintStreamRequest);
		}

		public Task<DataResponse> GetConfirmationDetailsAsync (GetConfirmationDetailsRequest GetConfirmationDetailsRequest)
		{
			return soapService.GetConfirmationDetailsAsync (GetConfirmationDetailsRequest);	
		}

		public Task<DataResponse> GetConcessionItemsListAsync (GetConcessionItemsListRequest GetConcessionItemsListRequest)
		{
			return soapService.GetConcessionItemsListAsync (GetConcessionItemsListRequest);
		}

		public Task<DataResponse> GetMovieShowtimesAsync (GetMovieShowtimesRequest GetMovieShowtimesRequest)
		{
			return soapService.GetMovieShowtimesAsync (GetMovieShowtimesRequest);
		}

		public Task<DataResponse> GetProductInfoAsync (GetProductInfoRequest GetProductInfoRequest)
		{
			return soapService.GetProductInfoAsync (GetProductInfoRequest);
		}

		public Task<DataResponse> GetClientListAsync (GetClientListRequest GetClientListRequest)
		{
			return soapService.GetClientListAsync (GetClientListRequest);
		}

		public Task<DataResponse> GiftCardBalanceRequestAsync (GiftCardBalanceRequest GCBalanceRequest)
		{
			return soapService.GiftCardBalanceRequestAsync (GCBalanceRequest);
		}

		public Task<Response> CreateClientAsync (CreateClientRequest CreateClientRequest)
		{
			return soapService.CreateClientAsync (CreateClientRequest);
		}

		public Task<Response> UpdateClientAsync (UpdateClientRequest UpdateClientRequest)
		{
			return soapService.UpdateClientAsync (UpdateClientRequest);
		}

		public Task<DataResponse> GetLtyMembershipConcessionItemAsync (GetLtyMembershipConcessionItemRequest GetLtyMembershipConcessionItemRequest)
		{
			return soapService.GetLtyMembershipConcessionItemAsync (GetLtyMembershipConcessionItemRequest);
		}

		public Task<DataResponse> GetConfigSettingListAsync (GetConfigSettingListRequest GetConfigSettingListRequest)
		{
			return soapService.GetConfigSettingListAsync (GetConfigSettingListRequest);
		}

		public Task<GetOrderHistoryResponse> GetOrderHistoryAsync (GetOrderHistoryRequest GetOrderHistoryRequest)
		{
			return soapService.GetOrderHistoryAsync (GetOrderHistoryRequest);
		}

		public Task<GetCinemaSiteGroupsResponse> GetCinemaSiteGroupsAsync (GetCinemaSiteGroupsRequest GetCinemaSiteGroupsRequest)
		{
			return soapService.GetCinemaSiteGroupsAsync (GetCinemaSiteGroupsRequest);
		}

		public Task<GetConcessionPrintStreamsResponse> GetConcessionPrintStreamsAsync (GetConcessionPrintStreamsRequest GetConcessionPrintStreamsRequest)
		{
			return soapService.GetConcessionPrintStreamsAsync (GetConcessionPrintStreamsRequest);
		}

		public Task<GetSessionSeatPlanResponse> GetSessionSeatPlanAsync (GetSessionSeatPlanRequest GetSessionSeatPlanRequest)
		{
			return soapService.GetSessionSeatPlanAsync (GetSessionSeatPlanRequest);
		}
	}
}

