﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using System.Threading.Tasks;
using System.Linq;
using Foundation;
using UIKit;

namespace AlbaCinema
{
	public partial class Horarios2Page : ContentPage
	{
		static string _Cinema_strID;
		static string _Movie_strID;

		private static Horarios2Page _page;

		public Horarios2Page (string Cinema_strID, string Movie_strID, string Movie_strName)
		{
			InitializeComponent ();

			_page = this;

			// Asignaciones
			_Cinema_strID = Cinema_strID;
			_Movie_strID = Movie_strID;
		}

		public static async Task<GetMovieShowtimesAsyncTable[]> Datos()
		{
			List<GetMovieShowtimesAsyncTable> peliculas = null;
			GetMovieShowtimesClass resultingMessage = null;

			try {
				DataResponse movies = await App.AlbaManager.GetMovieShowtimesAsync (new GetMovieShowtimesRequest () {
					BizDate = DateTime.Now.ToString ("yyyyMMdd") + "000000",
					//BizDate = "20160217000000",
					//BizStartTimeOfDay = 0,
					BizStartTimeOfDay = DateTime.Now.Hour,
					CinemaId = _Cinema_strID,
					OptionalClientClass = Constants.ClientClass,
					OrderMode = "MOVIES",
					AllSalesChannels = true
				});

				if (movies.Result == ResultCode.OK) {

					resultingMessage = Utils.DataResponseToObject<GetMovieShowtimesClass> (movies.DatasetXML);

					if (resultingMessage.Items != null) {
						var pelis = (from a in resultingMessage.Items.AsEnumerable()
								where a.Cinema_strID == _Cinema_strID
							&& a.Movie_strID == _Movie_strID
							select a).Distinct();

						peliculas = new List<GetMovieShowtimesAsyncTable>();

						foreach (var fila in pelis) {
							peliculas.Add (fila);
						}
					}
				} else {

					BasePage.MostrarAlerta (_page, true, null);
				}
			} catch (Exception ex) {
				
				BasePage.MostrarAlerta (_page, false, ex.Message);
			}

			return peliculas.OrderBy (p => p.Session_dtmDate_Time).ToArray ();;
		}

		public static async Task Navegar(GetMovieShowtimesAsyncTable item)
		{
			bool compra = Cines2Page.CompraEnLinea (new GetCinemaListAllAsyncTable () {
				Cinema_strID = item.Cinema_strID
			});

			if (compra) {
				//await _page.Navigation.PushAsync (new AsientosPage (item));
				await _page.Navigation.PushAsync (new EntradasPage (item));
			} else {
				await _page.Navigation.PushAsync (new Pelicula2Page(item.Cinema_strID, item.Movie_strID));
			}

		}
	}
}

