﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace AlbaCinema
{
	public partial class PromocionesPage : ContentPage
	{
		int Imagen;
		double x;

		public PromocionesPage ()
		{
			InitializeComponent ();

			Imagen = 1;
		}

		void onLogoTapped (object sender, EventArgs args)
		{
			if (Imagen < 3) {
				
				Imagen++;
			} else {
				
				Imagen = 1;
			}

			imgPromo.Source = ImageSource.FromFile ("Promos/" + Imagen + ".jpg");
		}

		private void CambiarFoto(int Siguiente)
		{
			Imagen += Siguiente;

			if (Imagen < 1) {
				Imagen = 3;
			} else  if (Imagen > 3){
				Imagen = 1;
			}

			imgPromo.Source = ImageSource.FromFile ("Promos/" + Imagen + ".jpg");
		}

		void onLogoPanned (object sender, PanUpdatedEventArgs e)
		{
			switch (e.StatusType) {
			case GestureStatus.Running:
				// Translate and ensure we don't pan beyond the wrapped user interface element bounds.
				x += e.TotalX;
				break;

			case GestureStatus.Completed:
				// Store the translation applied during the pan

				if (x >= 0) {
					CambiarFoto (1);
				} else {
					CambiarFoto (-1);
				}

				x = 0;

				break;
			}
		}
	}
}

