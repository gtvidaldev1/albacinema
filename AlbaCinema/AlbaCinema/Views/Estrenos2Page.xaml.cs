﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using System.Threading.Tasks;
using System.Linq;

namespace AlbaCinema
{
	public partial class Estrenos2Page : ContentPage
	{
		private static Estrenos2Page _page;

		public Estrenos2Page ()
		{
			InitializeComponent ();

			_page = this;
		}

		public static async Task<PeliculasClass[]> Datos()
		{
			List<PeliculasClass> peliculas = null;

			try {
				DataResponse movies = await App.AlbaManager.GetMovieShowtimesAsync (new GetMovieShowtimesRequest () {
					BizDate = DateTime.Now.ToString ("yyyyMMdd") + "000000",
					//BizDate = "20160217000000",
					//BizStartTimeOfDay = 0,
					BizStartTimeOfDay = DateTime.Now.Hour,
					OptionalClientClass = Constants.ClientClass,
					OrderMode = "MOVIES",
					AllSalesChannels = true
				});

				if (movies.Result == ResultCode.OK) {

					GetMovieShowtimesClass resultingMessage = Utils.DataResponseToObject<GetMovieShowtimesClass> (movies.DatasetXML);

					if (resultingMessage.Items != null) {
						var pelis = from a in resultingMessage.Items.AsEnumerable()
								where a.Film_dtmOpeningDate >= DateTime.Today.AddDays(-7)
							group a by new
						{
							Movie_strID = a.Movie_strID,
							Movie_strName = a.Movie_strName,
							Movie_strRating = a.Movie_strRating,
							Film_strURLforFilmName = a.Film_strURLforFilmName
						} into g
							select new PeliculasClass()
						{
							Movie_strID = g.Key.Movie_strID,
							Movie_strName = g.Key.Movie_strName,
							Movie_strRating = g.Key.Movie_strRating,
							Film_strURLforFilmName = g.Key.Film_strURLforFilmName,
							Horarios = g.AsEnumerable()
						};

						peliculas = new List<PeliculasClass>();

						foreach (var fila in pelis) {
							fila.Film_strURLforFilmName = Constants.PictureUrl + fila.Film_strURLforFilmName;
							peliculas.Add (fila);
						}
					}
				} else {
					
					BasePage.MostrarAlerta (_page, true, null);
				}
			} catch (Exception ex) {
				
				BasePage.MostrarAlerta (_page, false, ex.Message);
			}

			return peliculas.ToArray();
		}

		public static async Task Informacion(PeliculasClass item)
		{
			await _page.Navigation.PushAsync (new Pelicula2Page (item.Cinema_strID, item.Movie_strID));
		}
	}
}

