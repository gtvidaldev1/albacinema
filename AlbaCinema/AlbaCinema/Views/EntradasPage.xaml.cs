﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace AlbaCinema
{
	public partial class EntradasPage : ContentPage
	{
		GetMovieShowtimesAsyncTable _ShowTime;
		int Cantidad;
		decimal Precio;
		decimal Total;


		public EntradasPage (GetMovieShowtimesAsyncTable ShowTime)
		{
			InitializeComponent ();

			this._ShowTime = ShowTime;

			// boton del menu
			ToolbarItems.Add (new ToolbarItem ("Asientos", null, async () => {
				var page = new PagoPage();
				await Navigation.PushAsync(page);
			}));

			Cantidad = 1;
			Precio = 12.50M;
			Total = 123.50M;
		}

		void HandleValueChanged (object sender, EventArgs e)
		{   // display the value in a label

			var ex = (ValueChangedEventArgs)e;

			txtCantidad.Text = Math.Round( ex.NewValue, 0).ToString();// slider.Value.ToString ();
		}
	}
}

