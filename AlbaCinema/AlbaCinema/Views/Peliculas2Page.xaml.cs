﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using System.Threading.Tasks;
using System.Linq;

namespace AlbaCinema
{
	public partial class Peliculas2Page : ContentPage
	{
		private static Peliculas2Page _page;
		static string _Cinema_strID;

		public Peliculas2Page (string Cinema_strID, string Cinema_strName)
		{
			InitializeComponent ();

			_page = this;
			_Cinema_strID = Cinema_strID;
		}

		public static async Task<PeliculasClass[]> Datos()
		{
			List<PeliculasClass> peliculas = null;

			try {
				DataResponse movies = await App.AlbaManager.GetMovieShowtimesAsync (new GetMovieShowtimesRequest () {
					BizDate = DateTime.Now.ToString ("yyyyMMdd") + "000000",
					//BizDate = "20160217000000",
					//BizStartTimeOfDay = 0,
					BizStartTimeOfDay = DateTime.Now.Hour,
					CinemaId = _Cinema_strID,
					OptionalClientClass = Constants.ClientClass,
					OrderMode = "MOVIES",
					AllSalesChannels = true
				});

				if (movies.Result == ResultCode.OK) {

					GetMovieShowtimesClass resultingMessage = Utils.DataResponseToObject<GetMovieShowtimesClass> (movies.DatasetXML);

					if (resultingMessage.Items != null) {
						var pelis = from a in resultingMessage.Items.AsEnumerable()
							group a by new
						{
							Cinema_strID = a.Cinema_strID,
							Movie_strID = a.Movie_strID,
							Movie_strName = a.Movie_strName,
							Movie_strRating = a.Movie_strRating,
							Film_strURLforFilmName = a.Film_strURLforFilmName
						} into g
							select new PeliculasClass()
						{
							Cinema_strID = g.Key.Cinema_strID,
							Movie_strID = g.Key.Movie_strID,
							Movie_strName = g.Key.Movie_strName,
							Movie_strRating = g.Key.Movie_strRating,
							Film_strURLforFilmName = g.Key.Film_strURLforFilmName,
							Horarios = g.AsEnumerable()
						};

						peliculas = new List<PeliculasClass>();

						foreach (var fila in pelis) {
							fila.Film_strURLforFilmName = Constants.PictureUrl + fila.Film_strURLforFilmName;
							peliculas.Add (fila);
						}
					}
				} else {
					
					BasePage.MostrarAlerta (_page, true, null);
				}
			} catch (Exception ex) {
				
				BasePage.MostrarAlerta (_page, false, ex.Message);
			}

			return peliculas.ToArray();
		}

		public static async Task Informacion(PeliculasClass item)
		{
			await _page.Navigation.PushAsync (new Pelicula2Page (item.Cinema_strID, item.Movie_strID));
		}

		public static async Task Navegar(PeliculasClass item)
		{
			await _page.Navigation.PushAsync (new Horarios2Page (item.Cinema_strID, item.Movie_strID,
				item.Movie_strName));
		}
	}
}

