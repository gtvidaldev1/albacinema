﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using System.Threading.Tasks;

namespace AlbaCinema
{
	public partial class Cines2Page : ContentPage
	{
		private static Cines2Page _page;

		public Cines2Page ()
		{
			InitializeComponent ();

			_page = this;
		}

		public static async Task<GetCinemaListAllAsyncTable[]> Datos()
		{
			GetCinemaListAllClass resultingMessage = null;

			try {
				var movies = await App.AlbaManager.GetCinemaListAllAsync (new GetCinemaListAllRequest () {

				});

				if (movies.Result == ResultCode.OK) {

					resultingMessage = Utils.DataResponseToObject<GetCinemaListAllClass> (movies.DatasetXML);
				} else {
					
					BasePage.MostrarAlerta(_page, true, null);
				}
			} catch (Exception ex) {
				
				BasePage.MostrarAlerta (_page, false, ex.Message);
			}

			return resultingMessage.Items;
		}

		public static bool CompraEnLinea(GetCinemaListAllAsyncTable item)
		{
			if (item.Cinema_strID == "9983" || item.Cinema_strID == "9999"
				|| item.Cinema_strID == "9998" //|| item.Cinema_strID == "9986"
				|| item.Cinema_strID.StartsWith("000001")) {
				return true;
			} else {
				return false;
			}
		}

		public static async Task Navegar(GetCinemaListAllAsyncTable item)
		{
			await _page.Navigation.PushAsync (new Peliculas2Page (item.Cinema_strID, item.Cinema_strName));
		}
	}
}

