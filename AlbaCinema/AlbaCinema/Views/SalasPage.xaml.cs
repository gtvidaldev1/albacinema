﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using System.Collections.ObjectModel;

namespace AlbaCinema
{
	public partial class SalasPage : ContentPage
	{
		ObservableCollection<GetCinemaListAllAsyncTable> salas = new ObservableCollection<GetCinemaListAllAsyncTable>();
		//string translatedNumber;

		public SalasPage ()
		{
			InitializeComponent ();

			SalasView.ItemsSource = salas;

			SalasView.ItemSelected += OnItemSelected;
		}

		protected async override void OnAppearing ()
		{
			base.OnAppearing ();

			try {
				var movies = await App.AlbaManager.GetCinemaListAllAsync (new GetCinemaListAllRequest () {
				});

				if (movies.Result == ResultCode.OK) {

					GetCinemaListAllClass resultingMessage = Utils.DataResponseToObject<GetCinemaListAllClass> (movies.DatasetXML);

					foreach (var fila in resultingMessage.Items) {
						salas.Add (fila);
					}
				}
				else
				{
					await DisplayAlert (
						"No disponible",
						movies.Result.ToString(),
						"OK");
				}

				//CarteleraView.ItemsSource = respuesta;
			} catch (Exception ex) {
				await DisplayAlert (
					"Hosted Back-End",
					ex.ToString (),
					"OK");
			}
		}

		async void OnItemSelected (object sender, SelectedItemChangedEventArgs e)
		{
			var item = e.SelectedItem as GetCinemaListAllAsyncTable;
			if (item != null) {
				await Navigation.PushAsync (new PagoPage ());
			}
		}
	}
}

