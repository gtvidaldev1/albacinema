﻿using System;
using System.Collections.Generic;
using System.Linq;

using Xamarin.Forms;
using System.Threading.Tasks;
using Foundation;
using UIKit;

namespace AlbaCinema
{
	public partial class Ubicaciones2Page : ContentPage
	{
		private static Ubicaciones2Page _page;

		public Ubicaciones2Page ()
		{
			InitializeComponent ();

			_page = this;
		}

		public static void Llamar(GetCinemaListAllAsyncTable item)
		{
			var query = from a in AlbaContext.Cines ()
			            where a.Cinema_strID == item.Cinema_strID
			            select a;

			var url = new NSUrl ("tel:+502" + query.First().Cinema_strPhoneCountryCode);

			if (!UIApplication.SharedApplication.OpenUrl (url)) {
				var av = new UIAlertView ("No soportado",
					"Las llamadas no están soportadas en este dispositivo",
					null,
					"OK",
					null);
				av.Show ();
			};
		}

		public static void Navegar(GetCinemaListAllAsyncTable item)
		{
			bool waze = false;
			double? latitud = null;
			double? longitud = null;

			if (item.Cinema_decLatitudeSpecified && item.Cinema_decLongitudeSpecified) {
				latitud = item.Cinema_decLatitude;
				longitud = item.Cinema_decLongitude;
				waze = true;
			} else {
				var query = from a in AlbaContext.Cines ()
				           where a.Cinema_strID == item.Cinema_strID
				           select a;

				if (query.Count () > 0) {
					latitud = query.First ().Cinema_decLatitude;
					longitud = query.First ().Cinema_decLongitude;
					waze = true;
				}
			}

			if (waze) {
				Uri direccion;

				if (Device.OS == TargetPlatform.iOS) {
					direccion = new Uri ("waze://?ll=" + latitud.Value
					                + "," + longitud.Value + "&navigate=yes");
				} else {
					direccion = new Uri ("waze://waze/?ll=" + latitud.Value
						+ "," + longitud.Value + "&z=6&navigate=yes");
				}
				// waze://waze/?ll=52.123,-1.234&z=6&navigate=yes
				Device.OpenUri(direccion);
			} else {
				var av = new UIAlertView ("No disponible",
					"La dirección no está disponible",
					null,
					"OK",
					null);
				av.Show ();
			}
		}
	}
}

