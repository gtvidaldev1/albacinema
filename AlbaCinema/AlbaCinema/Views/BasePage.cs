﻿using System;

using Xamarin.Forms;

namespace AlbaCinema
{
	public partial class BasePage : ContentPage
	{
		public BasePage ()
		{

		}

		private bool _isBusy;

		public new bool IsBusy
		{
			get { return _isBusy; }
			set
			{
				_isBusy = value;
				OnPropertyChanged();
			}
		}

		public static async void MostrarAlerta(ContentPage page, bool Data, string Mensaje)
		{
			if (Data) {
				await page.DisplayAlert (
					"Datos no disponibles",
					"Los datos no están disponibles en este momento. Por favor, intente más tarde.",
					"OK");	
			} else {
				await page.DisplayAlert (
					"Ocurrió un problema",
					Mensaje,
					"OK");	
			}
		}

	}
}


