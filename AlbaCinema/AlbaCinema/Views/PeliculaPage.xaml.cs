﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using System.Linq;

namespace AlbaCinema
{
	public partial class PeliculaPage : ContentPage
	{
		//string id;

		public PeliculaPage (string id)
		{
			InitializeComponent ();
		
			//this.id = id;
			this.BindingContext = null;

			this.btnComprar.Clicked +=  BtnComprar_Clicked;
		}

		protected async override void OnAppearing ()
		{
			base.OnAppearing ();

			try {
				var movies = await App.AlbaManager.GetMovieInfoListAsync (new GetMovieInfoListRequest () {
					OptionalClientClass = Constants.ClientClass,
					OptionalTypeFlag = MovieTypeFlag.NotSet,
				});

				if (movies.Result == ResultCode.OK) {

					GetMovieInfoListClass resultingMessage = Utils.DataResponseToObject<GetMovieInfoListClass> (movies.DatasetXML);

					this.BindingContext = resultingMessage;
				}
				else
				{
					await DisplayAlert (
						"No disponible",
						movies.Result.ToString(),
						"OK");
				}

				//CarteleraView.ItemsSource = respuesta;
			} catch (Exception ex) {
				await DisplayAlert (
					"Hosted Back-End",
					ex.ToString (),
					"OK");
			}
		}

		async void BtnComprar_Clicked (object sender, EventArgs e)
		{
			await Navigation.PushAsync (new CinesPage ());
		}
	}
}

