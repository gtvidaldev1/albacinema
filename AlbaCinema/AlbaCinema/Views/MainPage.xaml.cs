﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace AlbaCinema
{
	public partial class MainPage : MasterDetailPage
	{
		MasterPage masterPage;

		public MainPage ()
		{
			InitializeComponent ();

			if (Device.OS == TargetPlatform.Android) {
				this.BackgroundColor = Color.FromHex ("#222222");
			}

			masterPage = new MasterPage ();
			Master = masterPage;
			Detail = new NavigationPage (new Cines2Page ());

			masterPage.ListView.ItemSelected += OnItemSelected;
		}

		void OnItemSelected (object sender, SelectedItemChangedEventArgs e)
		{
			var item = e.SelectedItem as MasterPageItem;

			if (item != null) {
				Detail = new NavigationPage ((Page)Activator.CreateInstance (item.TargetType));
				masterPage.ListView.SelectedItem = null;
				IsPresented = false;
			}
		}
	}
}

