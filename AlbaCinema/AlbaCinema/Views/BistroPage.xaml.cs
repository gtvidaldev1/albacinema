﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using System.Collections.ObjectModel;

namespace AlbaCinema
{
	public partial class BistroPage : ContentPage
	{
		int Imagen;
		double x;

		public BistroPage ()
		{
			InitializeComponent ();

			Imagen = 1;
		}

		private void CambiarFoto(int Siguiente)
		{
			Imagen += Siguiente;

			if (Imagen < 1) {
				Imagen = 7;
			} else  if (Imagen > 7){
				Imagen = 1;
			}

			imgPromo.Source = ImageSource.FromFile ("Bistro/" + Imagen + ".jpg");
		}

		void onLogoPanned (object sender, PanUpdatedEventArgs e)
		{
			switch (e.StatusType) {
			case GestureStatus.Running:
				// Translate and ensure we don't pan beyond the wrapped user interface element bounds.
				x += e.TotalX;
				break;

			case GestureStatus.Completed:
				// Store the translation applied during the pan

				if (x >= 0) {
					CambiarFoto (1);
				} else {
					CambiarFoto (-1);
				}

				x = 0;

				break;
			}
		}
	}
}

