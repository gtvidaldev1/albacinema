﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using UIKit;

namespace AlbaCinema
{
	public partial class AcercaPage : ContentPage
	{
		public AcercaPage ()
		{
			InitializeComponent ();
		}

		void onLogoTapped (object sender, EventArgs args)
		{
			// Visitar el sitio Web
			Uri url = new Uri ("http://www.vidal.gt");

			Device.OpenUri (url);
		}

		public void onLinkClicked(object sender, EventArgs args)
		{
			// Mandar correo			
			string texto = ((Button)sender).Text;

			Uri url = new Uri ("mailto:" + texto);

			Device.OpenUri (url);
		}

		protected override void OnAppearing ()
		{
			base.OnAppearing ();

			imgAlba.WidthRequest = Bounds.Width / 2;
			imgVidal.WidthRequest = Bounds.Width / 3;
		}
	}
}

