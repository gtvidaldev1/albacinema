﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace AlbaCinema
{
	public partial class CinesPage : ContentPage
	{
		ObservableCollection<GetCinemaListAllAsyncTable> peliculas = new ObservableCollection<GetCinemaListAllAsyncTable> ();

		public CinesPage ()
		{
			InitializeComponent ();

			this.Title = "Cines";
			if (Device.OS == TargetPlatform.iOS) {
				this.BackgroundColor = Color.FromHex ("#222222");
			}
			else {
				this.BackgroundColor = Color.FromHex ("#222222");
			}

			CarteleraView.ItemsSource = peliculas;
			CarteleraView.ItemSelected += OnItemSelected;
		}

		protected async override void OnAppearing ()
		{
			base.OnAppearing ();

			peliculas.Clear ();

			try {
				var movies = await App.AlbaManager.GetCinemaListAllAsync (new GetCinemaListAllRequest () {
					
				});

				if (movies.Result == ResultCode.OK) {

					GetCinemaListAllClass resultingMessage = Utils.DataResponseToObject<GetCinemaListAllClass> (movies.DatasetXML);

					foreach (var fila in resultingMessage.Items) {
						peliculas.Add (fila);
					}
				} else {
					await DisplayAlert (
						"No disponible",
						"No hay datos para mostrar",
						"OK");
				}
			} catch (Exception ex) {
				await DisplayAlert (
					"Hosted Back-End",
					ex.Message,
					"OK");
			}
		}

		async void OnItemSelected (object sender, SelectedItemChangedEventArgs e)
		{
			var item = e.SelectedItem as GetCinemaListAllAsyncTable;
			if (item != null) {
				
				await Navigation.PushAsync (new CarteleraPage (item.Cinema_strID, item.Cinema_strName));
			}
		}
			
	}
}

