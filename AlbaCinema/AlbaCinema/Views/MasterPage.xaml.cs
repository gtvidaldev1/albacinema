﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace AlbaCinema
{
	public partial class MasterPage : ContentPage
	{
		public ListView ListView { get { return listView; } }

		public MasterPage ()
		{
			InitializeComponent ();

			if (Device.OS == TargetPlatform.iOS) {
				this.BackgroundColor = Color.FromHex ("#404040");
			}
			else {
				this.BackgroundColor = Color.FromHex ("#404040");
			}

			var masterPageItems = new List<MasterPageItem> ();
			masterPageItems.Add (new MasterPageItem{
				Title = "Cartelera",
				IconSource = "Film.png",
				TargetType = typeof(Cines2Page)
			});
			masterPageItems.Add (new MasterPageItem{
				Title = "Bistro",
				IconSource = "Ticket.png",
				TargetType = typeof(BistroPage)
			});
			masterPageItems.Add (new MasterPageItem{
				Title = "Estrenos",
				IconSource = "StarredTicket.png",
				TargetType = typeof(Estrenos2Page)
			});
			masterPageItems.Add (new MasterPageItem{
				Title = "Promociones",
				IconSource = "Discount.png",
				TargetType = typeof(PromocionesPage)
			});
			masterPageItems.Add (new MasterPageItem{
				Title = "Dulcería",
				IconSource = "Audience.png",
				TargetType = typeof(DulceriaPage)
			});
			masterPageItems.Add (new MasterPageItem{
				Title = "Ubicaciones",
				IconSource = "News.png",
				TargetType = typeof(Ubicaciones2Page)
			});
			masterPageItems.Add (new MasterPageItem{
				Title = "Acerca de",
				IconSource = "AskQuestion.png",
				TargetType = typeof(AcercaPage)
			});

			listView.ItemsSource = masterPageItems;
		}
	}
}

