﻿using System;
using System.Collections.Generic;
using System.Linq;

using Xamarin.Forms;
using System.Threading.Tasks;

namespace AlbaCinema
{
	public partial class Pelicula2Page : ContentPage
	{
		private static Pelicula2Page _page;
		protected static string _Cinema_strID;
		protected static string _Movie_strID;

		public Pelicula2Page (string Cinema_strID, string Movie_strID)
		{
			InitializeComponent ();

			_page = this;
			_Cinema_strID = Cinema_strID;
			_Movie_strID = Movie_strID;
		}

		public static async Task<TuplaClass[]> Datos()
		{
			GetMovieListTable peliculas = null;
			List<TuplaClass> lst = null;

			try {
				DataResponse movies = await App.AlbaManager.GetMovieShowtimesAsync (new GetMovieShowtimesRequest () {
					BizDate = DateTime.Now.ToString ("yyyyMMdd") + "000000",
					//BizDate = "20160217000000",
					//BizStartTimeOfDay = 0,
					CinemaId = _Cinema_strID,
					BizStartTimeOfDay = DateTime.Now.Hour,
					OptionalClientClass = Constants.ClientClass,
					OrderMode = "MOVIES",
					AllSalesChannels = true,
				});

				if (movies.Result == ResultCode.OK) {

					GetMovieShowtimesClass resultingMessage = Utils.DataResponseToObject<GetMovieShowtimesClass> (movies.DatasetXML);

					if (resultingMessage.Items != null) {
						var query = (from a in resultingMessage.Items.AsEnumerable()
								where a.Movie_strID == _Movie_strID
							select a).FirstOrDefault();

						lst = new List<TuplaClass>();
						lst.Add(new TuplaClass("Nombre", query.Film_strTitle));
						lst.Add(new TuplaClass("Sinopsis", query.Film_strDescriptionLong));
						lst.Add(new TuplaClass("Rating", query.Film_strCensor));
						lst.Add(new TuplaClass("Categoría", query.FilmCat_strName));
						lst.Add(new TuplaClass("Apertura", query.Film_dtmOpeningDate.ToString("dd/MM/yyyy")));
						lst.Add(new TuplaClass("Año", query.Film_intOpeningYear.ToString()));
						lst.Add(new TuplaClass("Contenido", query.Film_strContent));
						lst.Add(new TuplaClass("Duración", query.Film_intDuration.ToString() + " min"));
						lst.Add(new TuplaClass("Formato", Convert.ToString(query.Film_strMovieFormat)));
						lst.Add(new TuplaClass("Formato de sonido", Convert.ToString(query.Film_strSoundFormat)));

					}
				} else {
					
					BasePage.MostrarAlerta (_page, true, null);
				}
			} catch (Exception ex) {
				
				BasePage.MostrarAlerta (_page, false, ex.Message);
			}

			return lst.ToArray();
		}
	}
}

