﻿using System;
using System.Collections.Generic;
using System.Linq;

using Xamarin.Forms;
using AlbaCinema.iOS;
using UIKit;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace AlbaCinema
{
	public partial class AsientosPage : ContentPage
	{
		private static AsientosPage _page;
		private bool _isBusy;
		private GetMovieShowtimesAsyncTable _ShowTime;
		private Vista.AddTicketsResponse tickets;
		private Guid _UserId;
		private Vista.Theatre teatro;
		private TapGestureRecognizer tapGestureRecognizer;

		public AsientosPage (GetMovieShowtimesAsyncTable ShowTime)
		{
			InitializeComponent ();

			_page = this;
			_isBusy = true;

			this.Title = ShowTime.Screen_strName;
			this._ShowTime = ShowTime;


			// boton del menu
			/*ToolbarItems.Add (new ToolbarItem ("Pagar", null, async () => {
				var page = new PagoPage();
				await Navigation.PushAsync(page);
			}));*/

			this.BindingContext = this;
			_UserId = Guid.NewGuid ();
		}

		public new bool IsBusy
		{
			get { return _isBusy; }
			set
			{
				_isBusy = value;
				OnPropertyChanged();
			}
		}

		protected async override void OnAppearing ()
		{
			base.OnAppearing ();

			List<TuplaClass> lst = new List<TuplaClass> ();
			lst.Add (new TuplaClass ("Disponible", "chairbrown.png"));
			lst.Add (new TuplaClass ("Reservado", "chairred.png"));
			lst.Add (new TuplaClass ("Seleccionado", "chairgreen.png"));


			//LeyendaView.ItemsSource = lst;

			try {
				var types = await App.AlbaManager.GetTicketTypeListAsync(new GetTicketTypeListRequest()
					{
						SessionId = _ShowTime.Session_strID,
						CinemaId = _ShowTime.Cinema_strID,
						OptionalClientClass = Constants.ClientClass
					});

				GetTicketTypeListClass tiposTicket = Utils.DataResponseToObject<GetTicketTypeListClass> (types.DatasetXML);


				tickets = await App.TicketingManager.AddTickets(new Vista.AddTicketsRequest()
					{
						BookingMode = 0,
						CinemaId = _ShowTime.Cinema_strID,
						SessionId = _ShowTime.Session_strID,
						UserSessionId = _UserId.ToString().Replace("-", string.Empty),
						ReturnDiscountInfo = true,
						ReturnOrder = true,
						ReturnSeatData = true,
						ReturnSeatDataFormat = 2,
						TicketTypes = new Vista.TicketType[]{
							new Vista.TicketType()
							{
								PriceInCents = Convert.ToInt32( tiposTicket.Table.Price_intTicket_Price),
								Qty = 5,
								TicketTypeCode = tiposTicket.Table.Price_strTicket_Type_Code,
							}
						},
						OptionalClientClass = Constants.ClientClass
					});
				
				if (tickets != null && tickets.SeatLayoutData != null) {

					teatro = tickets.SeatLayoutData;

					Image asiento;
					Label etiqueta;

					RowDefinition miRow = new RowDefinition () {
						Height = new GridLength (32, GridUnitType.Absolute)
					};

					ColumnDefinition miCol = new ColumnDefinition () {
						Width = new GridLength (32, GridUnitType.Absolute)
					};

					tapGestureRecognizer = new TapGestureRecognizer ();
					tapGestureRecognizer.Tapped += OnTapGestureRecognizerTapped;

					for (int i = 1; i <= teatro.Areas[0].ColumnCount + 2; i++) {

						GridAsientos.ColumnDefinitions.Add (miCol);
					}

					for (int i = 1; i <= teatro.Areas[0].RowCount; i++) {

						// Nueva fila
						GridAsientos.RowDefinitions.Add (miRow);

						string NombreFisico = teatro.Areas[0].Rows[i - 1].PhysicalName;

						if (string.IsNullOrWhiteSpace(NombreFisico))
						{
							continue;
						}

						// Columna con el nombre
						etiqueta = new Label ()
						{
							Text = NombreFisico,
							TextColor = Color.White,
							HorizontalTextAlignment = TextAlignment.Center,
							VerticalTextAlignment = TextAlignment.Center
						};

						GridAsientos.Children.Add (etiqueta, 0, i - 1);

						etiqueta = new Label ()
						{
							Text = NombreFisico,
							TextColor = Color.White,
							HorizontalTextAlignment = TextAlignment.Center,
							VerticalTextAlignment = TextAlignment.Center
						};

						GridAsientos.Children.Add (etiqueta, teatro.Areas[0].ColumnCount + 1, i - 1);

						// Todos los asientos de una fila
					
						foreach(var seat in teatro.Areas[0].Rows[i - 1].Seats)
						{
							string sauce = "chair";

							if (seat.Status == Vista.SeatStatus.Sold ||seat.Status == Vista.SeatStatus.Broken)
							{
								sauce += "red";
							}
							else if (seat.Status == Vista.SeatStatus.Reserved)
							{
								sauce += "green";
							}
							else
							{
								sauce += "brown";
							}


							asiento = new Image ()
							{
								Source = sauce + ".png"
							};
							asiento.GestureRecognizers.Add (tapGestureRecognizer);

							GridAsientos.Children.Add (asiento, seat.Position.ColumnIndex + 1, seat.Position.RowIndex);
						}
					}

					// Pantalla
					miRow = new RowDefinition () {
						Height = new GridLength (1, GridUnitType.Auto)
					};

					GridAsientos.RowDefinitions.Add (miRow);

					etiqueta = new Label ();
					etiqueta.Text = "PANTALLA";
					etiqueta.TextColor = Color.White;
					etiqueta.BackgroundColor = Color.Accent;
					etiqueta.HorizontalTextAlignment = TextAlignment.Center;
					etiqueta.VerticalTextAlignment = TextAlignment.Center;

					GridAsientos.Children.Add (etiqueta, 1, teatro.Areas[0].RowCount + 1);

					Grid.SetColumnSpan(etiqueta, teatro.Areas[0].ColumnCount);



				} else {
					
					BasePage.MostrarAlerta (_page, true, null);
				}

			} catch (Exception ex) {
				BasePage.MostrarAlerta (_page, false,  ex.Message);
			}

			IsBusy = false;
		}

		async void OnTapGestureRecognizerTapped (object sender, EventArgs args)
		{
			Device.BeginInvokeOnMainThread (() => {
				IsBusy = true;
			});

			Image imageSender = (Image)sender;
			string nuevo = null;

			int Columna = Grid.GetColumn (imageSender);
			int Fila = Grid.GetRow (imageSender);
			List<Vista.SelectedSeat1> nuevos = null;

			switch (((FileImageSource)imageSender.Source).File) {

			case "chairbrown.png":
				/*if (tickets.SeatLayoutData.AreaCategories [0].SelectedSeats.Count () >= 5) {
					break;
				}*/

				nuevo = "chairgreen.png";

				nuevos = (from a in tickets.SeatLayoutData.AreaCategories [0].SelectedSeats
				          select new Vista.SelectedSeat1 () {
					AreaCategoryCode = tickets.SeatLayoutData.AreaCategories [0].AreaCategoryCode,
					AreaNumber = a.AreaNumber,
					ColumnIndex = a.ColumnIndex,
					RowIndex = a.RowIndex
				}).ToList ();

				nuevos.Last ().ColumnIndex = Columna - 1;
				nuevos.Last ().RowIndex = Fila;
				/*
				nuevos.Add (new Vista.SelectedSeat1 () {
					AreaCategoryCode = tickets.SeatLayoutData.AreaCategories [0].AreaCategoryCode,
					AreaNumber = tickets.SeatLayoutData.AreaCategories[0].SelectedSeats[0].AreaNumber,
					ColumnIndex = Columna - 1,
					RowIndex = Fila
				});*/
					
				break;
			case "chairgreen.png":
				if (tickets.SeatLayoutData.AreaCategories [0].SelectedSeats.Count () <= 1) {
					break;
				}

				nuevo = "chairbrown.png";
				/*
				//asientos.RemoveAll (p => p.ColumnIndex == Columna && p.RowIndex == Fila);
				nuevos = (from a in tickets.SeatLayoutData.AreaCategories [0].SelectedSeats
				          select new Vista.SelectedSeat1 () {
					AreaCategoryCode = tickets.SeatLayoutData.AreaCategories [0].AreaCategoryCode,
					AreaNumber = a.AreaNumber,
					ColumnIndex = a.ColumnIndex,
					RowIndex = a.RowIndex
				}).ToList ();

				nuevos.RemoveAll (p => p.ColumnIndex == Columna && p.RowIndex == Fila);
				*/
				break;
			default:
				// No pasa nada
				break;
			}

			Device.BeginInvokeOnMainThread (() => {
				IsBusy = false;
			});

			if (nuevos != null) {
				var query = await App.TicketingManager.SetSelectedSeats (new Vista.SetSelectedSeatsRequest () {
					CinemaId = _ShowTime.Cinema_strID,
					ReturnOrder = true,
					SessionId = _ShowTime.Session_strID,
					UserSessionId = _UserId.ToString ().Replace ("-", string.Empty),
					OptionalClientClass = Constants.ClientClass,
					SelectedSeats = nuevos.ToArray ()
				});

				if (query.Result == Vista.ResultCode.OK) {
					var neworder = await App.TicketingManager.GetSessionSeatData (new Vista.GetSessionSeatDataRequest () {
						CinemaId = _ShowTime.Cinema_strID,
						SessionId = _ShowTime.Session_strID,
						UserSessionId = _UserId.ToString ().Replace ("-", string.Empty)
					});

					if (neworder.Result == Vista.ResultCode.OK) {
						teatro = neworder.SeatLayoutData;
					}
				}

				if (nuevo != null) {
					ColorearAsientos ();
				}
			}
		}

		async void OnItemSelected (object sender, SelectedItemChangedEventArgs e)
		{
				await Navigation.PushAsync(new PagoPage());
		}

		private void ColorearAsientos()
		{
			Device.BeginInvokeOnMainThread (() => {
				IsBusy = true;
			});

			Image asiento;

			foreach (var child in GridAsientos.Children) {

				if (child is Image) {
					int col = Grid.GetColumn (child);
					int row = Grid.GetRow (child);

					var seat = teatro.Areas [0].Rows [row].Seats
						.Where (p => p.Position.ColumnIndex == col - 1)
						.FirstOrDefault();

					if (seat != null) {
						asiento = (Image)child;
						string sauce = "chair";

						if (seat.Status == Vista.SeatStatus.Sold || seat.Status == Vista.SeatStatus.Broken) {
							sauce += "red";
						} else if (seat.Status == Vista.SeatStatus.Reserved) {
							sauce += "green";
						} else {
							sauce += "brown";
						}

						sauce = sauce + ".png";

						if (((FileImageSource)asiento.Source).File != sauce) {
							
							asiento.Source = sauce;
						}
					} else {
						int f = 0;
					}
				}
			}

			Device.BeginInvokeOnMainThread (() => {
				IsBusy = false;
			});
		}
	}
}

