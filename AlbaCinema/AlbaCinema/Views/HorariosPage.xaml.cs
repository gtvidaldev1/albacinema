﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using System.Collections.ObjectModel;
using System.Linq;

namespace AlbaCinema
{
	public partial class HorariosPage : ContentPage
	{
		string _Cinema_strID;
		string _Movie_strID;
		ObservableCollection<GetMovieShowtimesAsyncTable> peliculas = new ObservableCollection<GetMovieShowtimesAsyncTable> ();

		public HorariosPage (string Cinema_strID, string Movie_strID, string Movie_strName)
		{
			InitializeComponent ();

			this.Title = Movie_strName;

			if (Device.OS == TargetPlatform.iOS) {
				this.BackgroundColor = Color.FromHex ("#222222");
			} else {
				this.BackgroundColor = Color.FromHex ("#222222");
			}

			// Asignaciones
			_Cinema_strID = Cinema_strID;
			_Movie_strID = Movie_strID;
			CarteleraView.ItemsSource = peliculas;

			//Eventos
			CarteleraView.ItemSelected += OnItemSelected;
		}

		protected async override void OnAppearing ()
		{
			base.OnAppearing ();

			peliculas.Clear ();

			try {
				DataResponse movies = await App.AlbaManager.GetMovieShowtimesAsync (new GetMovieShowtimesRequest () {
					BizDate = DateTime.Now.ToString ("yyyyMMdd") + "000000",
					//BizDate = "20160217000000",
					//BizStartTimeOfDay = 0,
					BizStartTimeOfDay = DateTime.Now.Hour,
					CinemaId = _Cinema_strID,
					OptionalClientClass = Constants.ClientClass,
					OrderMode = "MOVIES",
					AllSalesChannels = true
				});

				if (movies.Result == ResultCode.OK) {

					GetMovieShowtimesClass resultingMessage = Utils.DataResponseToObject<GetMovieShowtimesClass> (movies.DatasetXML);

					if (resultingMessage.Items != null) {
						var pelis = from a in resultingMessage.Items.AsEnumerable()
								where a.Cinema_strID == this._Cinema_strID
							&& a.Movie_strID == this._Movie_strID
							select a;

						foreach (var fila in pelis) {
							peliculas.Add (fila);
						}
					}
				} else {
					await DisplayAlert (
						"No disponible",
						"No hay datos para mostrar",
						"OK");
				}

			} catch (Exception ex) {
				await DisplayAlert (
					"Hosted Back-End",
					ex.Message,
					"OK");
			}
		}

		async void OnItemSelected (object sender, SelectedItemChangedEventArgs e)
		{
			var item = e.SelectedItem as GetMovieShowtimesAsyncTable;
			if (item != null) {

				await Navigation.PushAsync (new AsientosPage (item));
			}
		}
	}
}

