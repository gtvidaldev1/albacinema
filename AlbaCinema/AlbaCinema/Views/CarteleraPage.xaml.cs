﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace AlbaCinema
{
	public partial class CarteleraPage : ContentPage
	{
		string _Cinema_strID;
		ObservableCollection<PeliculasClass> peliculas = new ObservableCollection<PeliculasClass> ();
		private static CarteleraPage _page;

		public CarteleraPage (string Cinema_strID, string Cinema_strName)
		{
			InitializeComponent ();

			_page = this;
			this.Title = Cinema_strName;

			if (Device.OS == TargetPlatform.iOS) {
				this.BackgroundColor = Color.FromHex ("#222222");
			} else {
				this.BackgroundColor = Color.FromHex ("#222222");
			}

			// Asignaciones
			_Cinema_strID = Cinema_strID;
			CarteleraView.ItemsSource = peliculas;

			//Eventos
			CarteleraView.ItemSelected += OnItemSelected;
		}

		protected async override void OnAppearing ()
		{
			base.OnAppearing ();

			peliculas.Clear ();

			// GetCinemaListAllAsync
			// <Cinema_strID>9983</Cinema_strID>

			// GetCinemaListAsync
			// OptionalCinemaId = "9983"
			/*
			var yo = await App.AlbaManager.GetClientListAsync (new GetClientListRequest () {
			});*/


			try {
				/*
				var movies = await App.AlbaManager.GetMovieListAsync (new GetMovieListRequest () {
					OptionalClientClass = Constants.ClientClass,
					OptionalCinemaId = _Cinema_strID
				});*/

				/* si sirve
				var movies = await App.AlbaManager.GetMovieInfoListAsync(new GetMovieInfoListRequest()
					{
						OptionalClientClass = Constants.ClientClass
					});*/

				DataResponse movies = await App.AlbaManager.GetMovieShowtimesAsync (new GetMovieShowtimesRequest () {
					BizDate = DateTime.Now.ToString ("yyyyMMdd") + "000000",
					//BizDate = "20160217000000",
					//BizStartTimeOfDay = 0,
					BizStartTimeOfDay = DateTime.Now.Hour,
					CinemaId = _Cinema_strID,
					OptionalClientClass = Constants.ClientClass,
					OrderMode = "MOVIES",
					AllSalesChannels = true
				});

				// area innova - diconine
				// renovar educacion con tecnologia
				// digeduca investigacion, nivel de los gradundos mate y lenguage
				// empresarios por la educacion - donacion, capacitacion, infraestructura (financieros de libros(
				// 
				// 

				if (movies.Result == ResultCode.OK) {

					GetMovieShowtimesClass resultingMessage = Utils.DataResponseToObject<GetMovieShowtimesClass> (movies.DatasetXML);
						
					if (resultingMessage.Items != null) {
						var pelis = from a in resultingMessage.Items.AsEnumerable()
							group a by new
						{
							Cinema_strID = a.Cinema_strID,
							Movie_strID = a.Movie_strID,
							Movie_strName = a.Movie_strName,
							Movie_strRating = a.Movie_strRating,
							Film_strURLforFilmName = a.Film_strURLforFilmName
						} into g
							select new PeliculasClass()
						{
							Cinema_strID = g.Key.Cinema_strID,
							Movie_strID = g.Key.Movie_strID,
							Movie_strName = g.Key.Movie_strName,
							Movie_strRating = g.Key.Movie_strRating,
							Film_strURLforFilmName = g.Key.Film_strURLforFilmName,
							Horarios = g.AsEnumerable()
						};

						foreach (var fila in pelis) {
							fila.Film_strURLforFilmName = Constants.PictureUrl + fila.Film_strURLforFilmName;
							peliculas.Add (fila);
						}
					}
				} else {
					
					BasePage.MostrarAlerta (_page, true, null);
				}

			} catch (Exception ex) {
				
				BasePage.MostrarAlerta (_page, false,  ex.Message);
			}
		}


		async void OnItemSelected (object sender, SelectedItemChangedEventArgs e)
		{
			var item = e.SelectedItem as AlbaCinema.PeliculasClass;
			if (item != null) {

				await Navigation.PushAsync (new HorariosPage (item.Cinema_strID, item.Movie_strID,
					item.Movie_strName));
			}
		}
	}
}

