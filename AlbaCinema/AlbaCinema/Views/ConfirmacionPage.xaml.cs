﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using UIKit;
using EventKit;
using Foundation;

namespace AlbaCinema
{
	public partial class ConfirmacionPage : ContentPage
	{
		UITextView txtTarjeta;
		UIButton buttonPay;
		private static ConfirmacionPage _page;

		public ConfirmacionPage (string Number, string Month, string Year, string CVC,
			string Nombre, string Correo)
		{
			InitializeComponent ();

			_page = this;
			this.Title = "Confirmación de pago";
			if (Device.OS == TargetPlatform.Android) {
				this.BackgroundColor = Color.FromHex ("#222222");
			}

			txtNumber.Text = Number;
			txtMonth.Text = Month;
			txtYear.Text = Year;
			txtCVC.Text = CVC;
			txtNombre.Text = Nombre;
			txtCorreo.Text = Correo;

			// boton del menu
			ToolbarItems.Add (new ToolbarItem ("Pagar", null, async () => {
				await DisplayAlert("Ticket(s) comprados", "Su transacción ha sido exitosa. Recibirá su código por correo electrónico", "Aceptar");
				var page = new CinesPage();

				await Navigation.PushAsync(page);
			}));
		}

		protected async override void OnAppearing ()
		{
			base.OnAppearing ();

			try {
				var movies = await App.AlbaManager.GetCinemaListAllAsync (new GetCinemaListAllRequest () {

				});

				if (movies.Result == ResultCode.OK) {

					GetCinemaListAllClass resultingMessage = Utils.DataResponseToObject<GetCinemaListAllClass> (movies.DatasetXML);

					foreach (var fila in resultingMessage.Items) {
						//peliculas.Add (fila);
					}
				} else {

					BasePage.MostrarAlerta(_page, true, null);
				}
			} catch (Exception ex) {
				
				BasePage.MostrarAlerta (_page, false, ex.Message);
				
			}

			EKEvent newEvent = EKEvent.FromStore ( App.Current.EventStore );
			// set the alarm for 10 minutes from now
			newEvent.AddAlarm ( EKAlarm.FromDate( DateTimeToNSDate(DateTime.Now.AddMinutes(10))));
			// make the event start 20 minutes from now and last 30 minutes
			newEvent.StartDate =  DateTimeToNSDate(DateTime.Now.AddMinutes ( 20 ));
			newEvent.EndDate = DateTimeToNSDate(DateTime.Now.AddMinutes ( 50 ));
			newEvent.Title = "Get outside and do some exercise!";
			newEvent.Notes = "This is your motivational event to go and do 30 minutes of exercise. Super important. Do this.";

			newEvent.Calendar = App.Current.EventStore.DefaultCalendarForNewEvents;

			NSError e;
			App.Current.EventStore.SaveEvent ( newEvent, EKSpan.ThisEvent, out e );

			var av = new UIAlertView ("Evento guardado",
				e.Description,
				null,
				"OK",
				null);

			av.Show ();

			App.TicketingManager.CompleteOrder(new Vista.CompleteOrderRequest()
				{

				});
		}

		public static NSDate DateTimeToNSDate(DateTime date)
		{
			DateTime reference = TimeZone.CurrentTimeZone.ToLocalTime(
				new DateTime(2001, 1, 1, 0, 0, 0) );
			return NSDate.FromTimeIntervalSinceReferenceDate(
				(date - reference).TotalSeconds);
		}
	}
}

