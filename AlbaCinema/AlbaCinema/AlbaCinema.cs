﻿using System;

using Xamarin.Forms;
using EventKit;

namespace AlbaCinema
{
	public class App : Application
	{
		public static DataManager AlbaManager { get; set; }
		public static TicketingManager TicketingManager { get; set; }

		public App ()
		{
			// The root page of your application
			/*MainPage = new ContentPage {
				Content = new StackLayout {
					VerticalOptions = LayoutOptions.Center,
					Children = {
						new Label {
							XAlign = TextAlignment.Center,
							Text = "Welcome to Xamarin Forms!"
						}
					}
				}
			};*/

			//eventStore = new EKEventStore ( );

			MainPage = new AlbaCinema.MainPage ();
		}

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}

		public static new App Current {
			get { return current; }
		}

		private static App current;

		public EKEventStore EventStore {
			get { return eventStore; }
		}

		protected EKEventStore eventStore;

		static App ()
		{
			current = new App();
		}
	}
}

