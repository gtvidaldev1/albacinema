#pragma warning disable 1591
// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 4.0.30319.17020
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------

[assembly: Android.Runtime.ResourceDesignerAttribute("AlbaCinema.Droid.Resource", IsApplication=true)]

namespace AlbaCinema.Droid
{
	
	
	[System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Android.Build.Tasks", "1.0.0.0")]
	public partial class Resource
	{
		
		static Resource()
		{
			global::Android.Runtime.ResourceIdManager.UpdateIdValues();
		}
		
		public static void UpdateIdValues()
		{
			global::Stripe.Resource.Drawable.amex = global::AlbaCinema.Droid.Resource.Drawable.amex;
			global::Stripe.Resource.Drawable.credit = global::AlbaCinema.Droid.Resource.Drawable.credit;
			global::Stripe.Resource.Drawable.cvc = global::AlbaCinema.Droid.Resource.Drawable.cvc;
			global::Stripe.Resource.Drawable.diners = global::AlbaCinema.Droid.Resource.Drawable.diners;
			global::Stripe.Resource.Drawable.discover = global::AlbaCinema.Droid.Resource.Drawable.discover;
			global::Stripe.Resource.Drawable.jcb = global::AlbaCinema.Droid.Resource.Drawable.jcb;
			global::Stripe.Resource.Drawable.maestro = global::AlbaCinema.Droid.Resource.Drawable.maestro;
			global::Stripe.Resource.Drawable.mastercard = global::AlbaCinema.Droid.Resource.Drawable.mastercard;
			global::Stripe.Resource.Drawable.visa = global::AlbaCinema.Droid.Resource.Drawable.visa;
			global::Stripe.Resource.Id.stripeCvc = global::AlbaCinema.Droid.Resource.Id.stripeCvc;
			global::Stripe.Resource.Id.stripeExpiration = global::AlbaCinema.Droid.Resource.Id.stripeExpiration;
			global::Stripe.Resource.Id.stripeImage = global::AlbaCinema.Droid.Resource.Id.stripeImage;
			global::Stripe.Resource.Id.stripeLastFour = global::AlbaCinema.Droid.Resource.Id.stripeLastFour;
			global::Stripe.Resource.Id.stripeLayoutOne = global::AlbaCinema.Droid.Resource.Id.stripeLayoutOne;
			global::Stripe.Resource.Id.stripeLayoutTwo = global::AlbaCinema.Droid.Resource.Id.stripeLayoutTwo;
			global::Stripe.Resource.Id.stripeNumber = global::AlbaCinema.Droid.Resource.Id.stripeNumber;
			global::Stripe.Resource.Layout.stripeviewlayout = global::AlbaCinema.Droid.Resource.Layout.stripeviewlayout;
			global::Stripe.Resource.String.library_name = global::AlbaCinema.Droid.Resource.String.library_name;
		}
		
		public partial class Attribute
		{
			
			static Attribute()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private Attribute()
			{
			}
		}
		
		public partial class Drawable
		{
			
			// aapt resource value: 0x7f020000
			public const int amex = 2130837504;
			
			// aapt resource value: 0x7f020001
			public const int Armchair = 2130837505;
			
			// aapt resource value: 0x7f020002
			public const int ArmchairGreen = 2130837506;
			
			// aapt resource value: 0x7f020003
			public const int ArmchairRed = 2130837507;
			
			// aapt resource value: 0x7f020004
			public const int AskQuestion = 2130837508;
			
			// aapt resource value: 0x7f020005
			public const int Audience = 2130837509;
			
			// aapt resource value: 0x7f020006
			public const int credit = 2130837510;
			
			// aapt resource value: 0x7f020007
			public const int cvc = 2130837511;
			
			// aapt resource value: 0x7f020008
			public const int diners = 2130837512;
			
			// aapt resource value: 0x7f020009
			public const int Discount = 2130837513;
			
			// aapt resource value: 0x7f02000a
			public const int discover = 2130837514;
			
			// aapt resource value: 0x7f02000b
			public const int Film = 2130837515;
			
			// aapt resource value: 0x7f02000c
			public const int icon = 2130837516;
			
			// aapt resource value: 0x7f02000d
			public const int jcb = 2130837517;
			
			// aapt resource value: 0x7f02000e
			public const int maestro = 2130837518;
			
			// aapt resource value: 0x7f02000f
			public const int mastercard = 2130837519;
			
			// aapt resource value: 0x7f020010
			public const int Menu = 2130837520;
			
			// aapt resource value: 0x7f020011
			public const int News = 2130837521;
			
			// aapt resource value: 0x7f020012
			public const int StarredTicket = 2130837522;
			
			// aapt resource value: 0x7f020013
			public const int Ticket = 2130837523;
			
			// aapt resource value: 0x7f020014
			public const int visa = 2130837524;
			
			static Drawable()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private Drawable()
			{
			}
		}
		
		public partial class Id
		{
			
			// aapt resource value: 0x7f050006
			public const int stripeCvc = 2131034118;
			
			// aapt resource value: 0x7f050005
			public const int stripeExpiration = 2131034117;
			
			// aapt resource value: 0x7f050000
			public const int stripeImage = 2131034112;
			
			// aapt resource value: 0x7f050004
			public const int stripeLastFour = 2131034116;
			
			// aapt resource value: 0x7f050001
			public const int stripeLayoutOne = 2131034113;
			
			// aapt resource value: 0x7f050003
			public const int stripeLayoutTwo = 2131034115;
			
			// aapt resource value: 0x7f050002
			public const int stripeNumber = 2131034114;
			
			static Id()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private Id()
			{
			}
		}
		
		public partial class Layout
		{
			
			// aapt resource value: 0x7f030000
			public const int stripeviewlayout = 2130903040;
			
			static Layout()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private Layout()
			{
			}
		}
		
		public partial class String
		{
			
			// aapt resource value: 0x7f040000
			public const int library_name = 2130968576;
			
			static String()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private String()
			{
			}
		}
	}
}
#pragma warning restore 1591
