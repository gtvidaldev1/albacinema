﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Net;

namespace AlbaCinema.Droid
{
	public class SoapService : ISoapService
	{
		ws.DataService dataService;

		public SoapService ()
		{
			ServicePointManager.ServerCertificateValidationCallback = delegate {
				return true;
			};

			dataService = new ws.DataService (Constants.SoapUrl);
		}

		public async Task<DataResponse> GetCinemaListAsync (GetCinemaListRequest GetCinemaListRequest)
		{
			ws.GetCinemaListRequest model = new ws.GetCinemaListRequest () {
				OptionalBizDate = GetCinemaListRequest.OptionalBizDate,
				OptionalBizStartTimeOfDay = GetCinemaListRequest.OptionalBizStartTimeOfDay,
				OptionalCinemaId = GetCinemaListRequest.OptionalCinemaId,
				OptionalClientClass = GetCinemaListRequest.OptionalClientClass,
				OptionalIncludeGiftStores = GetCinemaListRequest.OptionalIncludeGiftStores,
				OptionalIncludeOperator = GetCinemaListRequest.OptionalIncludeOperator,
				OptionalMovieName = GetCinemaListRequest.OptionalMovieName,
				OptionalOperatorCode = GetCinemaListRequest.OptionalOperatorCode,
				OptionalOrderByOperator = GetCinemaListRequest.OptionalOrderByOperator,
			};

			DataResponse dataResponse = null;

			try {
				var result = await Task<ws.DataResponse>.Factory.FromAsync (
					dataService.BeginGetCinemaList, dataService.EndGetCinemaList,
					model, TaskCreationOptions.None);

				dataResponse = new DataResponse () {
					DatasetXML = result.DatasetXML,
					Result = (ResultCode)result.Result
				};
			} catch (Exception ex) {
				Debug.WriteLine (@" ERROR {0}", ex.Message);
			}

			return dataResponse;
		}

		public async Task<DataResponse> GetCinemaListAllAsync (GetCinemaListAllRequest GetCinemaListAllRequest)
		{
			ws.GetCinemaListAllRequest model = new ws.GetCinemaListAllRequest () {
				OptionalIncludeGiftStores = GetCinemaListAllRequest.OptionalIncludeGiftStores,
				OptionalIncludeOperator = GetCinemaListAllRequest.OptionalIncludeOperator,
				OptionalOrderByOperator = GetCinemaListAllRequest.OptionalOrderByOperator,
			};

			DataResponse dataResponse = null;

			try {
				var result = await Task<ws.DataResponse>.Factory.FromAsync (
					dataService.BeginGetCinemaListAll, dataService.EndGetCinemaListAll,
					model, TaskCreationOptions.None);

				dataResponse = new DataResponse () {
					DatasetXML = result.DatasetXML,
					Result = (ResultCode)result.Result
				};
			} catch (Exception ex) {
				Debug.WriteLine (@" ERROR {0}", ex.Message);
			}

			return dataResponse;
		}

		public async Task<DataResponse> GetMovieListAsync (GetMovieListRequest GetMovieListRequest)
		{
			ws.GetMovieListRequest model = new ws.GetMovieListRequest () {
				OptionalBizDate = GetMovieListRequest.OptionalBizDate,
				OptionalBizStartTimeOfDay = GetMovieListRequest.OptionalBizStartTimeOfDay,
				OptionalCinemaId = GetMovieListRequest.OptionalCinemaId,
				OptionalClientClass = GetMovieListRequest.OptionalClientClass,
				OptionalIncludeGiftStores = GetMovieListRequest.OptionalIncludeGiftStores,
				OptionalOperatorCode = GetMovieListRequest.OptionalOperatorCode,
				OptionalOrderByOperator = GetMovieListRequest.OptionalOrderByOperator,
			};

			DataResponse dataResponse = null;

			try {
				var result = await Task<ws.DataResponse>.Factory.FromAsync (
					dataService.BeginGetMovieList, dataService.EndGetMovieList,
					model, TaskCreationOptions.None);

				dataResponse = new DataResponse () {
					DatasetXML = result.DatasetXML,
					Result = (ResultCode)result.Result
				};
			} catch (Exception ex) {
				Debug.WriteLine (@" ERROR {0}", ex.Message);
			}

			return dataResponse;
		}

		public async Task<DataResponse> GetMovieInfoListAsync (GetMovieInfoListRequest GetMovieInfoListRequest)
		{
			ws.GetMovieInfoListRequest model = new ws.GetMovieInfoListRequest () {
				OptionalClientClass = GetMovieInfoListRequest.OptionalClientClass,
				OptionalMovieName = GetMovieInfoListRequest.OptionalMovieName,
				OptionalTypeFlag = (ws.MovieTypeFlag)GetMovieInfoListRequest.OptionalTypeFlag,
			};

			DataResponse dataResponse = null;

			try {
				var result = await Task<ws.DataResponse>.Factory.FromAsync (
					dataService.BeginGetMovieInfoList, dataService.EndGetMovieInfoList,
					model, TaskCreationOptions.None);

				dataResponse = new DataResponse () {
					DatasetXML = result.DatasetXML,
					Result = (ResultCode)result.Result
				};
			} catch (Exception ex) {
				Debug.WriteLine (@" ERROR {0}", ex.Message);
			}

			return dataResponse;
		}

		public async Task<DataResponse> GetMoviePeopleAsync (GetMoviePeopleRequest GetMoviePeopleRequest)
		{
			ws.GetMoviePeopleRequest model = new ws.GetMoviePeopleRequest () {
				MovieId = GetMoviePeopleRequest.MovieId,
			};

			DataResponse dataResponse = null;

			try {
				var result = await Task<ws.DataResponse>.Factory.FromAsync (
					dataService.BeginGetMoviePeople, dataService.EndGetMoviePeople,
					model, TaskCreationOptions.None);

				dataResponse = new DataResponse () {
					DatasetXML = result.DatasetXML,
					Result = (ResultCode)result.Result
				};
			} catch (Exception ex) {
				Debug.WriteLine (@" ERROR {0}", ex.Message);
			}

			return dataResponse;
		}

		public async Task<DataResponse> GetSessionListAsync (GetSessionListRequest GetSessionListRequest)
		{
			ws.GetSessionListRequest model = new ws.GetSessionListRequest () {
				CinemaId = GetSessionListRequest.CinemaId,
				MovieId = GetSessionListRequest.MovieId,
				OptionalBizDate = GetSessionListRequest.OptionalBizDate,
				OptionalBizStartHourOfDay = GetSessionListRequest.OptionalBizStartHourOfDay,
				OptionalClientClass = GetSessionListRequest.OptionalClientClass,
				OptionalEventCode = GetSessionListRequest.OptionalEventCode,
				OptionalMovieId = GetSessionListRequest.MovieId,
				OptionalMovieName = GetSessionListRequest.OptionalMovieName,
				OptionalOperatorCode = GetSessionListRequest.OptionalOperatorCode,
				OptionalSessionDisplayCutOff = GetSessionListRequest.OptionalSessionDisplayCutOff,
			};

			DataResponse dataResponse = null;

			try {
				var result = await Task<ws.DataResponse>.Factory.FromAsync (
					dataService.BeginGetSessionList, dataService.EndGetSessionList,
					model, TaskCreationOptions.None);

				dataResponse = new DataResponse () {
					DatasetXML = result.DatasetXML,
					Result = (ResultCode)result.Result
				};
			} catch (Exception ex) {
				Debug.WriteLine (@" ERROR {0}", ex.Message);
			}

			return dataResponse;
		}

		public async Task<DataResponse> GetSessionInfoAsync (GetSessionInfoRequest GetSessionInfoRequest)
		{
			ws.GetSessionInfoRequest model = new ws.GetSessionInfoRequest () {
				CinemaId = GetSessionInfoRequest.CinemaId,
				SessionId = GetSessionInfoRequest.SessionId,
			};

			DataResponse dataResponse = null;

			try {
				var result = await Task<ws.DataResponse>.Factory.FromAsync (
					dataService.BeginGetSessionInfo, dataService.EndGetSessionInfo,
					model, TaskCreationOptions.None);

				dataResponse = new DataResponse () {
					DatasetXML = result.DatasetXML,
					Result = (ResultCode)result.Result
				};
			} catch (Exception ex) {
				Debug.WriteLine (@" ERROR {0}", ex.Message);
			}

			return dataResponse;
		}

		public async Task<DataResponse> GetTicketTypeListAsync (GetTicketTypeListRequest GetTicketTypeListRequest)
		{
			ws.GetTicketTypeListRequest model = new ws.GetTicketTypeListRequest () {
				CinemaId = GetTicketTypeListRequest.CinemaId,
				SessionId = GetTicketTypeListRequest.SessionId,
				OptionalAreaCategoryCode = GetTicketTypeListRequest.OptionalAreaCategoryCode,
				OptionalClientClass = GetTicketTypeListRequest.OptionalClientClass,
				OptionalEnforceChildTicketLogic = GetTicketTypeListRequest.OptionalEnforceChildTicketLogic,
				OptionalIncludeZeroValueTickets = GetTicketTypeListRequest.OptionalIncludeZeroValueTickets,
				OptionalLoyaltyTicketMatchesHOCode = GetTicketTypeListRequest.OptionalLoyaltyTicketMatchesHOCode,
				OptionalReturnAllLoyaltyTickets = GetTicketTypeListRequest.OptionalReturnAllLoyaltyTickets,
				OptionalReturnAllRedemptionAndCompTickets = GetTicketTypeListRequest.OptionalReturnAllRedemptionAndCompTickets,
				OptionalReturnLoyaltyRewardFlag = GetTicketTypeListRequest.OptionalReturnLoyaltyRewardFlag,
				OptionalSeparatePaymentBasedTickets = GetTicketTypeListRequest.OptionalSeparatePaymentBasedTickets,
				OptionalShowLoyaltyTicketsToNonMembers = GetTicketTypeListRequest.OptionalShowLoyaltyTicketsToNonMembers,
				OptionalShowNonATMTickets = GetTicketTypeListRequest.OptionalShowNonATMTickets,
				OptionalUserSessionIdForLoyaltyTickets = GetTicketTypeListRequest.OptionalUserSessionIdForLoyaltyTickets,
			};

			DataResponse dataResponse = null;

			try {
				var result = await Task<ws.DataResponse>.Factory.FromAsync (
					dataService.BeginGetTicketTypeList, dataService.EndGetTicketTypeList,
					model, TaskCreationOptions.None);

				dataResponse = new DataResponse () {
					DatasetXML = result.DatasetXML,
					Result = (ResultCode)result.Result
				};
			} catch (Exception ex) {
				Debug.WriteLine (@" ERROR {0}", ex.Message);
			}

			return dataResponse;
		}

		public async Task<DataResponse> GetTicketTypeFromBarcodeAsync (GetTicketTypeFromBarcodeRequest GetTicketTypeFromBarcodeRequest)
		{
			ws.GetTicketTypeFromBarcodeRequest model = new ws.GetTicketTypeFromBarcodeRequest () {
				Barcode = GetTicketTypeFromBarcodeRequest.Barcode,
				CinemaId = GetTicketTypeFromBarcodeRequest.CinemaId,
				SessionId = GetTicketTypeFromBarcodeRequest.SessionId,
			};

			DataResponse dataResponse = null;

			try {
				var result = await Task<ws.DataResponse>.Factory.FromAsync (
					dataService.BeginGetTicketTypeFromBarcode, dataService.EndGetTicketTypeFromBarcode,
					model, TaskCreationOptions.None);

				dataResponse = new DataResponse () {
					DatasetXML = result.DatasetXML,
					Result = (ResultCode)result.Result
				};
			} catch (Exception ex) {
				Debug.WriteLine (@" ERROR {0}", ex.Message);
			}

			return dataResponse;
		}

		public async Task<DataResponse> GetTicketTypePackageAsync (GetTicketTypePackageRequest GetTicketTypePackageRequest)
		{
			ws.GetTicketTypePackageRequest model = new ws.GetTicketTypePackageRequest () {
				CinemaId = GetTicketTypePackageRequest.CinemaId,
				SessionId = GetTicketTypePackageRequest.SessionId,
				TicketTypeCode = GetTicketTypePackageRequest.TicketTypeCode,
			};

			DataResponse dataResponse = null;

			try {
				var result = await Task<ws.DataResponse>.Factory.FromAsync (
					dataService.BeginGetTicketTypePackage, dataService.EndGetTicketTypePackage,
					model, TaskCreationOptions.None);

				dataResponse = new DataResponse () {
					DatasetXML = result.DatasetXML,
					Result = (ResultCode)result.Result
				};
			} catch (Exception ex) {
				Debug.WriteLine (@" ERROR {0}", ex.Message);
			}

			return dataResponse;
		}

		public async Task<DataResponse> GetEventListAsync (GetEventListRequest GetEventListRequest)
		{
			ws.GetEventListRequest model = new ws.GetEventListRequest () {
				CinemaId = GetEventListRequest.CinemaId,
				OptionalClientClass = GetEventListRequest.OptionalClientClass,
				OptionalEventCode = GetEventListRequest.OptionalEventCode,
				OptionalMovieName = GetEventListRequest.OptionalMovieName,
				OptionalOperatorCode = GetEventListRequest.OptionalOperatorCode,
				TypeFlag = (ws.MovieTypeFlag)GetEventListRequest.TypeFlag,
			};

			DataResponse dataResponse = null;

			try {
				var result = await Task<ws.DataResponse>.Factory.FromAsync (
					dataService.BeginGetEventList, dataService.EndGetEventList,
					model, TaskCreationOptions.None);

				dataResponse = new DataResponse () {
					DatasetXML = result.DatasetXML,
					Result = (ResultCode)result.Result
				};
			} catch (Exception ex) {
				Debug.WriteLine (@" ERROR {0}", ex.Message);
			}

			return dataResponse;
		}

		public async Task<DataResponse> GetEventMovieListAsync (GetEventMovieListRequest GetEventMovieListRequest)
		{
			ws.GetEventMovieListRequest model = new ws.GetEventMovieListRequest () {

				CinemaId = GetEventMovieListRequest.CinemaId,
				EventCode = GetEventMovieListRequest.EventCode,
				OptionalClientClass = GetEventMovieListRequest.OptionalClientClass,
			};

			DataResponse dataResponse = null;

			try {
				var result = await Task<ws.DataResponse>.Factory.FromAsync (
					dataService.BeginGetEventMovieList, dataService.EndGetEventMovieList,
					model, TaskCreationOptions.None);

				dataResponse = new DataResponse () {
					DatasetXML = result.DatasetXML,
					Result = (ResultCode)result.Result
				};
			} catch (Exception ex) {
				Debug.WriteLine (@" ERROR {0}", ex.Message);
			}

			return dataResponse;
		}

		public async Task<DataResponse> GetShowtimeDateListAsync (GetShowtimeDateListRequest GetShowtimeDateListRequest)
		{
			ws.GetShowtimeDateListRequest model = new ws.GetShowtimeDateListRequest () {

				CinemaId = GetShowtimeDateListRequest.CinemaId,
				OptionalBizStartHourOfDay = GetShowtimeDateListRequest.OptionalBizStartHourOfDay,
			};

			DataResponse dataResponse = null;

			try {
				var result = await Task<ws.DataResponse>.Factory.FromAsync (
					dataService.BeginGetShowtimeDateList, dataService.EndGetShowtimeDateList,
					model, TaskCreationOptions.None);

				dataResponse = new DataResponse () {
					DatasetXML = result.DatasetXML,
					Result = (ResultCode)result.Result
				};
			} catch (Exception ex) {
				Debug.WriteLine (@" ERROR {0}", ex.Message);
			}

			return dataResponse;
		}

		public async Task<DataResponse> GetCinemaOpForSessionAsync (GetCinemaOpForSessionRequest GetCinemaOpForSessionRequest)
		{
			ws.GetCinemaOpForSessionRequest model = new ws.GetCinemaOpForSessionRequest () {
				CinemaId = GetCinemaOpForSessionRequest.CinemaId,
				SessionId = GetCinemaOpForSessionRequest.SessionId
			};

			DataResponse dataResponse = null;

			try {
				var result = await Task<ws.DataResponse>.Factory.FromAsync (
					dataService.BeginGetCinemaOpForSession, dataService.EndGetCinemaOpForSession,
					model, TaskCreationOptions.None);

				dataResponse = new DataResponse () {
					DatasetXML = result.DatasetXML,
					Result = (ResultCode)result.Result
				};
			} catch (Exception ex) {
				Debug.WriteLine (@" ERROR {0}", ex.Message);
			}

			return dataResponse;
		}

		public async Task<DataResponse> GetPrintStreamAsync (GetPrintStreamRequest GetPrintStreamRequest)
		{
			ws.GetPrintStreamRequest model = new ws.GetPrintStreamRequest () {
				PrintDocumentCode = GetPrintStreamRequest.PrintDocumentCode,
				PrintDocumentType = (ws.PrintDocumentType)GetPrintStreamRequest.PrintDocumentType,
				UserSessionId = GetPrintStreamRequest.UserSessionId,
			};

			DataResponse dataResponse = null;

			try {
				var result = await Task<ws.DataResponse>.Factory.FromAsync (
					dataService.BeginGetPrintStream, dataService.EndGetPrintStream,
					model, TaskCreationOptions.None);

				dataResponse = new DataResponse () {
					DatasetXML = result.DatasetXML,
					Result = (ResultCode)result.Result
				};
			} catch (Exception ex) {
				Debug.WriteLine (@" ERROR {0}", ex.Message);
			}

			return dataResponse;
		}

		public async Task<DataResponse> GetConfirmationDetailsAsync (GetConfirmationDetailsRequest GetConfirmationDetailsRequest)
		{
			ws.GetConfirmationDetailsRequest model = new ws.GetConfirmationDetailsRequest () {
				CinemaId = GetConfirmationDetailsRequest.CinemaId,
				ConfirmationDetailsType = (ws.ConfirmationDetailsType)GetConfirmationDetailsRequest.ConfirmationDetailsType,
				OptionalHistoryID = GetConfirmationDetailsRequest.OptionalHistoryID,
				VistaBookingNumber = GetConfirmationDetailsRequest.VistaBookingNumber,
				VistaTransNumber = GetConfirmationDetailsRequest.VistaTransNumber,
			};

			DataResponse dataResponse = null;

			try {
				var result = await Task<ws.DataResponse>.Factory.FromAsync (
					dataService.BeginGetConfirmationDetails, dataService.EndGetConfirmationDetails,
					model, TaskCreationOptions.None);

				dataResponse = new DataResponse () {
					DatasetXML = result.DatasetXML,
					Result = (ResultCode)result.Result
				};
			} catch (Exception ex) {
				Debug.WriteLine (@" ERROR {0}", ex.Message);
			}

			return dataResponse;
		}

		public async Task<DataResponse> GetConcessionItemsListAsync (GetConcessionItemsListRequest GetConcessionItemsListRequest)
		{
			ws.GetConcessionItemsListRequest model = new ws.GetConcessionItemsListRequest () {
				CinemaId = GetConcessionItemsListRequest.CinemaId,
				ClientID = GetConcessionItemsListRequest.ClientID,
				OrderUserId = GetConcessionItemsListRequest.OrderUserId,
			};

			DataResponse dataResponse = null;

			try {
				var result = await Task<ws.DataResponse>.Factory.FromAsync (
					dataService.BeginGetConcessionItemsList, dataService.EndGetConcessionItemsList,
					model, TaskCreationOptions.None);

				dataResponse = new DataResponse () {
					DatasetXML = result.DatasetXML,
					Result = (ResultCode)result.Result
				};
			} catch (Exception ex) {
				Debug.WriteLine (@" ERROR {0}", ex.Message);
			}

			return dataResponse;
		}

		public async Task<DataResponse> GetMovieShowtimesAsync (GetMovieShowtimesRequest GetMovieShowtimesRequest)
		{
			ws.GetMovieShowtimesRequest model = new ws.GetMovieShowtimesRequest () {
				AllSalesChannels = GetMovieShowtimesRequest.AllSalesChannels,
				BizDate = GetMovieShowtimesRequest.BizDate,
				BizStartTimeOfDay = GetMovieShowtimesRequest.BizStartTimeOfDay,
				CinemaId = GetMovieShowtimesRequest.CinemaId,
				OptionalClientClass = GetMovieShowtimesRequest.OptionalClientClass,
				OptionalSessionDisplayCutOffInMins = GetMovieShowtimesRequest.OptionalSessionDisplayCutOffInMins,
				OrderMode = GetMovieShowtimesRequest.OrderMode,
			};

			DataResponse dataResponse = null;

			try {
				var result = await Task<ws.DataResponse>.Factory.FromAsync (
					dataService.BeginGetMovieShowtimes, dataService.EndGetMovieShowtimes,
					model, TaskCreationOptions.None);

				dataResponse = new DataResponse () {
					DatasetXML = result.DatasetXML,
					Result = (ResultCode)result.Result
				};
			} catch (Exception ex) {
				Debug.WriteLine (@" ERROR {0}", ex.Message);
			}

			return dataResponse;
		}

		public async Task<DataResponse> GetProductInfoAsync (GetProductInfoRequest GetProductInfoRequest)
		{
			ws.GetProductInfoRequest model = new ws.GetProductInfoRequest () {
				ProductCode = GetProductInfoRequest.ProductCode,
			};

			DataResponse dataResponse = null;

			try {
				var result = await Task<ws.DataResponse>.Factory.FromAsync (
					dataService.BeginGetProductInfo, dataService.EndGetProductInfo,
					model, TaskCreationOptions.None);

				dataResponse = new DataResponse () {
					DatasetXML = result.DatasetXML,
					Result = (ResultCode)result.Result
				};
			} catch (Exception ex) {
				Debug.WriteLine (@" ERROR {0}", ex.Message);
			}

			return dataResponse;
		}

		public async Task<DataResponse> GetClientListAsync (GetClientListRequest GetClientListRequest)
		{
			ws.GetClientListRequest model = new ws.GetClientListRequest () {
				OptionalClientDescription = GetClientListRequest.OptionalClientDescription,
				OptionalClientId = GetClientListRequest.OptionalClientId,
			};

			DataResponse dataResponse = null;

			try {
				var result = await Task<ws.DataResponse>.Factory.FromAsync (
					dataService.BeginGetClientList, dataService.EndGetClientList,
					model, TaskCreationOptions.None);

				dataResponse = new DataResponse () {
					DatasetXML = result.DatasetXML,
					Result = (ResultCode)result.Result
				};
			} catch (Exception ex) {
				Debug.WriteLine (@" ERROR {0}", ex.Message);
			}

			return dataResponse;
		}

		public async Task<DataResponse> GiftCardBalanceRequestAsync (GiftCardBalanceRequest GCBalanceRequest)
		{
			ws.GiftCardBalanceRequest model = new ws.GiftCardBalanceRequest () {
				CardBalance = GCBalanceRequest.CardBalance,
				CardExpiry = GCBalanceRequest.CardExpiry,
				CardExpiryMonth = GCBalanceRequest.CardExpiryMonth,
				CardExpiryYear = GCBalanceRequest.CardExpiryYear,
				CardNo = GCBalanceRequest.CardNo,
				CardType = GCBalanceRequest.CardType,
				CinemaID = GCBalanceRequest.CinemaID,
				UseGiftCardBalanceMethod = (ws.GiftCardBalanceMethod)GCBalanceRequest.UseGiftCardBalanceMethod,
			};

			DataResponse dataResponse = null;

			try {
				var result = await Task<ws.DataResponse>.Factory.FromAsync (
					dataService.BeginGiftCardBalanceRequest, dataService.EndGiftCardBalanceRequest,
					model, TaskCreationOptions.None);

				dataResponse = new DataResponse () {
					DatasetXML = result.DatasetXML,
					Result = (ResultCode)result.Result
				};
			} catch (Exception ex) {
				Debug.WriteLine (@" ERROR {0}", ex.Message);
			}

			return dataResponse;
		}

		public async Task<Response> CreateClientAsync (CreateClientRequest CreateClientRequest)
		{
			ws.CreateClientRequest model = new ws.CreateClientRequest () {
				ClientClass = CreateClientRequest.ClientClass,
				ClientId = CreateClientRequest.ClientId,
				Description = CreateClientRequest.Description,
				SalesChannel = CreateClientRequest.SalesChannel,
				WorkstationCode = CreateClientRequest.WorkstationCode,
			};

			Response response = null;

			try {
				var result = await Task<ws.Response>.Factory.FromAsync (
					dataService.BeginCreateClient, dataService.EndCreateClient,
					model, TaskCreationOptions.None);

				response = new Response () {
					ErrorDescription = result.ErrorDescription,
					Result = (ResultCode1)result.Result,
				};
			} catch (Exception ex) {
				Debug.WriteLine (@" ERROR {0}", ex.Message);
			}

			return response;
		}

		public async Task<Response> UpdateClientAsync (UpdateClientRequest UpdateClientRequest)
		{
			ws.UpdateClientRequest model = new ws.UpdateClientRequest () {
				ConfigXml = UpdateClientRequest.ConfigXml,
				Status = UpdateClientRequest.Status,
				ClientClass = UpdateClientRequest.ClientClass,
				ClientId = UpdateClientRequest.ClientId,
				Description = UpdateClientRequest.Description,
				SalesChannel = UpdateClientRequest.SalesChannel,
				WorkstationCode = UpdateClientRequest.WorkstationCode,
			};

			Response response = null;

			try {
				var result = await Task<ws.Response>.Factory.FromAsync (
					dataService.BeginUpdateClient, dataService.EndUpdateClient,
					model, TaskCreationOptions.None);

				response = new Response () {
					ErrorDescription = result.ErrorDescription,
					Result = (ResultCode1)result.Result,
				};
			} catch (Exception ex) {
				Debug.WriteLine (@" ERROR {0}", ex.Message);
			}

			return response;
		}

		public async Task<DataResponse> GetLtyMembershipConcessionItemAsync (GetLtyMembershipConcessionItemRequest GetLtyMembershipConcessionItemRequest)
		{
			ws.GetLtyMembershipConcessionItemRequest model = new ws.GetLtyMembershipConcessionItemRequest () {
				ClientID = GetLtyMembershipConcessionItemRequest.ClientID,
			};

			DataResponse dataResponse = null;

			try {
				var result = await Task<ws.DataResponse>.Factory.FromAsync (
					dataService.BeginGetLtyMembershipConcessionItem, dataService.EndGetLtyMembershipConcessionItem,
					model, TaskCreationOptions.None);

				dataResponse = new DataResponse () {
					DatasetXML = result.DatasetXML,
					Result = (ResultCode)result.Result
				};
			} catch (Exception ex) {
				Debug.WriteLine (@" ERROR {0}", ex.Message);
			}

			return dataResponse;
		}

		public async Task<DataResponse> GetConfigSettingListAsync (GetConfigSettingListRequest GetConfigSettingListRequest)
		{
			ws.GetConfigSettingListRequest model = new ws.GetConfigSettingListRequest () {
				SettingNames = GetConfigSettingListRequest.SettingNames,
			};

			DataResponse dataResponse = null;

			try {
				var result = await Task<ws.DataResponse>.Factory.FromAsync (
					dataService.BeginGetConfigSettingList, dataService.EndGetConfigSettingList,
					model, TaskCreationOptions.None);

				dataResponse = new DataResponse () {
					DatasetXML = result.DatasetXML,
					Result = (ResultCode)result.Result
				};
			} catch (Exception ex) {
				Debug.WriteLine (@" ERROR {0}", ex.Message);
			}

			return dataResponse;
		}

		public async Task<GetOrderHistoryResponse> GetOrderHistoryAsync (GetOrderHistoryRequest GetOrderHistoryRequest)
		{
			ws.GetOrderHistoryRequest model = new ws.GetOrderHistoryRequest () {
				LoyaltyMemberId = GetOrderHistoryRequest.LoyaltyMemberId,
				MemberEmail = GetOrderHistoryRequest.MemberEmail,
			};

			GetOrderHistoryResponse dataResponse = null;

			try {
				var result = await Task<ws.GetOrderHistoryResponse>.Factory.FromAsync (
					dataService.BeginGetOrderHistory, dataService.EndGetOrderHistory,
					model, TaskCreationOptions.None);

				dataResponse = new GetOrderHistoryResponse () {
					NonPackageTickets = (from a in result.NonPackageTickets
						select new OrderHistoryLine () {
							BookingFeeInCents = a.BookingFeeInCents,
							BookingNumber = a.BookingNumber,
							CinemaId = a.CinemaId,
							CinemaName = a.CinemaName,
							CinemaName2 = a.CinemaName2,
							InitiatedTime = a.InitiatedTime,
							IsPaid = a.IsPaid,
							MoveNameAlt = a.MoveNameAlt,
							MovieName = a.MovieName,
							PackageGroupNumber = a.PackageGroupNumber,
							PackageSeats = (from b in a.PackageSeats
								select new SeatInfo () {
									SeatNumber = b.SeatNumber,
									SeatRowId = b.SeatRowId,
								}).ToArray (),
							ParentTicketDescription = a.ParentTicketDescription,
							ParentTicketDescriptionAlt = a.ParentTicketDescriptionAlt,
							ParentTicketTypeCode = a.ParentTicketTypeCode,
							SeatNumber = a.SeatNumber,
							SeatRowId = a.SeatRowId,
							SessionTime = a.SessionTime,
							TicketDescription = a.TicketDescription,
							TicketDescriptionAlt = a.TicketDescriptionAlt,
							TicketPriceInCents = a.TicketPriceInCents,
						}).ToArray (),
					PackageTickets = (from a in result.PackageTickets
						select new OrderHistoryLine () {
							BookingFeeInCents = a.BookingFeeInCents,
							BookingNumber = a.BookingNumber,
							CinemaId = a.CinemaId,
							CinemaName = a.CinemaName,
							CinemaName2 = a.CinemaName2,
							InitiatedTime = a.InitiatedTime,
							IsPaid = a.IsPaid,
							MoveNameAlt = a.MoveNameAlt,
							MovieName = a.MovieName,
							PackageGroupNumber = a.PackageGroupNumber,
							PackageSeats = (from b in a.PackageSeats
								select new SeatInfo () {
									SeatNumber = b.SeatNumber,
									SeatRowId = b.SeatRowId,
								}).ToArray (),
							ParentTicketDescription = a.ParentTicketDescription,
							ParentTicketDescriptionAlt = a.ParentTicketDescriptionAlt,
							ParentTicketTypeCode = a.ParentTicketTypeCode,
							SeatNumber = a.SeatNumber,
							SeatRowId = a.SeatRowId,
							SessionTime = a.SessionTime,
							TicketDescription = a.TicketDescription,
							TicketDescriptionAlt = a.TicketDescriptionAlt,
							TicketPriceInCents = a.TicketPriceInCents,
						}).ToArray (),
					ErrorDescription = result.ErrorDescription,
					Result = (ResultCode1)result.Result,
				};
			} catch (Exception ex) {
				Debug.WriteLine (@" ERROR {0}", ex.Message);
			}

			return dataResponse;
		}

		public async Task<GetCinemaSiteGroupsResponse> GetCinemaSiteGroupsAsync (GetCinemaSiteGroupsRequest GetCinemaSiteGroupsRequest)
		{
			ws.GetCinemaSiteGroupsRequest model = new ws.GetCinemaSiteGroupsRequest () {
			};

			GetCinemaSiteGroupsResponse dataResponse = null;

			try {
				var result = await Task<ws.GetCinemaSiteGroupsResponse>.Factory.FromAsync (
					dataService.BeginGetCinemaSiteGroups, dataService.EndGetCinemaSiteGroups,
					model, TaskCreationOptions.None);

				dataResponse = new GetCinemaSiteGroupsResponse () {
					CinemaSiteGroupLinks = (from a in result.CinemaSiteGroupLinks
						select new CinemaSiteGroupLinkLine () {
							CinemaId = a.CinemaId,
							CinemaSiteGroupId = a.CinemaSiteGroupId,
						}).ToArray (),
					CinemaSiteGroups = (from a in result.CinemaSiteGroups
						select new CinemaSiteGroupLine () {
							AreaCode = a.AreaCode,
							CinemaSiteGroupId = a.CinemaSiteGroupId,
							Name = a.Name,
						}).ToArray (),
					ResultCode = result.ResultCode
				};
			} catch (Exception ex) {
				Debug.WriteLine (@" ERROR {0}", ex.Message);
			}

			return dataResponse;
		}

		public async Task<GetConcessionPrintStreamsResponse> GetConcessionPrintStreamsAsync (GetConcessionPrintStreamsRequest GetConcessionPrintStreamsRequest)
		{
			ws.GetConcessionPrintStreamsRequest model = new ws.GetConcessionPrintStreamsRequest () {
				CinemaId = GetConcessionPrintStreamsRequest.CinemaId,
				ItemCode = GetConcessionPrintStreamsRequest.ItemCode,
				SequenceNumber = GetConcessionPrintStreamsRequest.SequenceNumber,
				UserSessionId = GetConcessionPrintStreamsRequest.UserSessionId,
				VistaTransactionId = GetConcessionPrintStreamsRequest.VistaTransactionId,
			};

			GetConcessionPrintStreamsResponse dataResponse = null;

			try {
				var result = await Task<ws.GetConcessionPrintStreamsResponse>.Factory.FromAsync (
					dataService.BeginGetConcessionPrintStreams, dataService.EndGetConcessionPrintStreams,
					model, TaskCreationOptions.None);

				dataResponse = new GetConcessionPrintStreamsResponse () {
					ConcessionPrintStreams = (from a in result.ConcessionPrintStreams
						select new ConcessionPrintStream () {
							CinemaId = a.CinemaId,
							DocumentType = a.DocumentType,
							Id = a.Id,
							ItemCode = a.ItemCode,
							ItemSequence = a.ItemSequence,
							PrintStream = a.PrintStream,
						}).ToArray(),
				};
			} catch (Exception ex) {
				Debug.WriteLine (@" ERROR {0}", ex.Message);
			}

			return dataResponse;
		}

		public async Task<GetSessionSeatPlanResponse> GetSessionSeatPlanAsync (GetSessionSeatPlanRequest GetSessionSeatPlanRequest)
		{
			ws.GetSessionSeatPlanRequest model = new ws.GetSessionSeatPlanRequest () {
				CinemaId = GetSessionSeatPlanRequest.CinemaId,
				SessionId = GetSessionSeatPlanRequest.SessionId,
			};

			GetSessionSeatPlanResponse dataResponse = null;

			try {
				var result = await Task<ws.GetSessionSeatPlanResponse>.Factory.FromAsync (
					dataService.BeginGetSessionSeatPlan, dataService.EndGetSessionSeatPlan,
					model, TaskCreationOptions.None);

				dataResponse = new GetSessionSeatPlanResponse () {
					SeatLayoutData = new Theatre () {
						AreaCategories = (from a in result.SeatLayoutData.AreaCategories
							select new AreaCategory () {
								AreaCategoryCode = a.AreaCategoryCode,
								SeatsAllocatedCount = a.SeatsAllocatedCount,
								SeatsNotAllocatedCount = a.SeatsNotAllocatedCount,
								SeatsToAllocate = a.SeatsToAllocate,
								SelectedSeats = (from b in a.SelectedSeats
									select new SeatPosition () {
										AreaNumber = b.AreaNumber,
										ColumnIndex = b.ColumnIndex,
										RowIndex = b.RowIndex,
									}).ToArray ()
							}).ToArray (),
						Areas = (from a in result.SeatLayoutData.Areas
							select new Area () {
								AreaCategoryCode = a.AreaCategoryCode,
								ColumnCount = a.ColumnCount,
								Description = a.Description,
								DescriptionAlt = a.DescriptionAlt,
								HasSofaSeatingEnabled = a.HasSofaSeatingEnabled,
								Height = a.Height,
								IsAllocatedSeating = a.IsAllocatedSeating,
								Left = a.Left,
								Number = a.Number,
								NumberOfSeats = a.NumberOfSeats,
								RowCount = a.RowCount,
								Rows = (from b in a.Rows
									select new Row () {
										PhysicalName = b.PhysicalName,
										Seats = (from c in b.Seats
											select new Seat () {

											}).ToArray ()
									}).ToArray (),
								Top = a.Top,
								Width = a.Width,
							}).ToArray (),
						BoundaryLeft = result.SeatLayoutData.BoundaryLeft,
						BoundaryRight = result.SeatLayoutData.BoundaryRight,
						BoundaryTop = result.SeatLayoutData.BoundaryTop,
						ScreenStart = result.SeatLayoutData.ScreenStart,
						ScreenWidth = result.SeatLayoutData.ScreenWidth,
					},
				};
			} catch (Exception ex) {
				Debug.WriteLine (@" ERROR {0}", ex.Message);
			}

			return dataResponse;
		}
	}
}

